$key="96volt"
$filename = "C:\Source\CommonLib\VoltHelper\VoltHelper\VoltHelpers.nuspec"
[xml]$xml = Get-Content $filename
$version=write $xml.SelectNodes('//package/metadata/version').InnerText
$nugetURL="http://nugetserver.96volt.com/nuget"
$fileName=(-join("VoltHelper.",$version,".nupkg"))
$params=(-join("push ",$fileName," ",$key," -Source ",$nugetURL))

write "Pakowanie..."
& .\nuget.exe @("pack","VoltHelper.csproj","-Properties", "Configuration=Release")

write  "Wysy�anie..."
&  .\nuget.exe @("push", $fileName, $key, "-Source", $nugetURL,"-SkipDuplicate")
