﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Helper.Time
{
    public class TimeRange : ICloneable
    {
      //  public Tag TagObject { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public TimeSpan Difference { get { return TimeEnd - TimeStart; } }



        public TimeRange(DateTime timeStart, DateTime timesEnd/*, Tag tagObjec*/)
        {
            TimeEnd = timesEnd;
            //TagObject = tagObject;
            TimeStart = timeStart;
        }

        //public TimeRange(DateTime timeStart, DateTime timesEnd)
        //    : this(timeStart, timesEnd/*, default(Tag)*/)
        //{
        //}

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
