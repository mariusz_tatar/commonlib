﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Helper.Time
{
    public class TimeCommonRange
    {
        public delegate TResult CreateDelegate<TResult>(TimeRange rangeSlave, TimeRange rangeMaster, DateTime calculatedTimeStart, DateTime calculatedTimeEnd);

        public static IEnumerable<TResult> CalculateCommonRanges<TResult>(IEnumerable<TimeRange> rangesSlave, IEnumerable<TimeRange> rangesMaster, CreateDelegate<TResult> createNewElementFunction) where TResult : Time.TimeRange
        {
            if (rangesSlave == null || rangesSlave.Any() == false || rangesMaster == null || rangesMaster.Any() == false)
                yield break;

            foreach (var master in rangesMaster)
            {
                foreach (var slave in rangesSlave)
                {
                    if (slave.TimeStart > master.TimeEnd || slave.TimeEnd < master.TimeStart)
                        continue;

                    var commonRange = GetCommonRange(slave, master, createNewElementFunction);

                    if (commonRange != null)
                        yield return commonRange;
                }
            }

        }

        private static TResult GetCommonRange<TResult>(TimeRange currentRangeSlave, TimeRange master, CreateDelegate<TResult> createNewElementFunction) where TResult : Time.TimeRange
        {
            bool startBetween = currentRangeSlave.TimeStart.BetweenDates(master.TimeStart, master.TimeEnd);
            bool endBetween = currentRangeSlave.TimeEnd.BetweenDates(master.TimeStart, master.TimeEnd);
            TResult result = default(TResult);


            if (startBetween && endBetween)
            {
                //cały zakres
                result = createNewElementFunction(currentRangeSlave, master, currentRangeSlave.TimeStart, currentRangeSlave.TimeEnd);// (TimeRange<Tag1>)currentRangeSlave.Clone();
            }
            else if (startBetween)
            {
                //poczatek slave koniec master
                result = createNewElementFunction(currentRangeSlave, master, currentRangeSlave.TimeStart, master.TimeEnd); //new TimeRange<Tag1>(currentRangeSlave.TimeStart, master.TimeEnd);
            }
            else if (endBetween)
            {
                //poczatek master koniec slave
                result = createNewElementFunction(currentRangeSlave, master, master.TimeStart, currentRangeSlave.TimeEnd); //new TimeRange<Tag1>(master.TimeStart, currentRangeSlave.TimeEnd);
            }
            else if (currentRangeSlave.TimeStart < master.TimeStart && currentRangeSlave.TimeEnd > master.TimeEnd)
            {
                result = createNewElementFunction(currentRangeSlave, master, master.TimeStart, master.TimeEnd); //new TimeRange<Tag1>(master.TimeStart, master.TimeEnd);
            }

            if (result != null && result.Difference.Ticks > 0)
                return result;

            return null;
        }
    }
}
