﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Helper.Time
{
   public class SingleTime<Tag>
    {
        public Tag TagObject { get; set; }
        public DateTime TimeStart { get; set; }

        public SingleTime(DateTime timeStart)
        {
            TimeStart = timeStart;
        }
    }
}
