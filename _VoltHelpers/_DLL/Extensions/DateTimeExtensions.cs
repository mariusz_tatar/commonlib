﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class DateExtensions
{
    public static bool BetweenDates(this DateTime input, DateTime date1, DateTime date2)
    {
        if (date1.Ticks > date2.Ticks)
        {
            DateTime tmp = date2;
            date2 = date1;
            date1 = tmp;
        }

        return (input.Ticks >= date1.Ticks && input.Ticks <= date2.Ticks);
    }

    public static bool BetweenDatesDate2NotEqual(this DateTime input, DateTime date1, DateTime date2)
    {
        if (date1.Ticks > date2.Ticks)
        {
            DateTime tmp = date2;
            date2 = date1;
            date1 = tmp;
        }

        return (input.Ticks >= date1.Ticks && input.Ticks < date2.Ticks);
    }

    public static bool BetweenDatesWithNotEqual(this DateTime input, DateTime date1, DateTime date2)
    {
        if (date1.Ticks > date2.Ticks)
        {
            DateTime tmp = date2;
            date2 = date1;
            date1 = tmp;
        }

        return (input.Ticks > date1.Ticks && input.Ticks < date2.Ticks);
    }
}
