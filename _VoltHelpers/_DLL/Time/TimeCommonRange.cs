﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltHelpers.Time
{
    public class TimeCommonRange
    {
        public static IEnumerable<TimeRange<Tag1>> CalculateCommonRanges<Tag1, Tag2>(IEnumerable<TimeRange<Tag1>> rangesSlave, IEnumerable<TimeRange<Tag2>> rangesMaster)
        {
            if (rangesSlave == null || rangesSlave.Any() == false || rangesMaster == null || rangesMaster.Any() == false)
                yield break;

            foreach (var master in rangesMaster)
            {
                foreach (var slave in rangesSlave)
                {
                    if (slave.TimeStart > master.TimeEnd || slave.TimeEnd < master.TimeStart)
                        continue;

                    var commonRange = GetCommonRange(slave, master);

                    if (commonRange != null)
                        yield return commonRange;
                }
            }

        }

        private static TimeRange<Tag1> GetCommonRange<Tag1, Tag2>(TimeRange<Tag1> currentRangeSlave, TimeRange<Tag2> master)
        {
            bool startBetween = currentRangeSlave.TimeStart.BetweenDates(master.TimeStart, master.TimeEnd);
            bool endBetween = currentRangeSlave.TimeEnd.BetweenDates(master.TimeStart, master.TimeEnd);
            TimeRange<Tag1> result = null;


            if (startBetween && endBetween)
            {
                //cały zakres
                result= (TimeRange<Tag1>)currentRangeSlave.Clone();
            }
            else if (startBetween)
            {
                //poczatek slave koniec master
                result = new TimeRange<Tag1>(currentRangeSlave.TimeStart, master.TimeEnd);
            }
            else if (endBetween)
            {
                //poczatek master koniec slave
                result = new TimeRange<Tag1>(master.TimeStart, currentRangeSlave.TimeEnd);
            }
            else if (currentRangeSlave.TimeStart < master.TimeStart && currentRangeSlave.TimeEnd > master.TimeEnd)
            {
                result = new TimeRange<Tag1>(master.TimeStart, master.TimeEnd);
            }

            if (result != null && result.Difference.Ticks > 0)
                return result;

            return null;
        }
    }
}
