﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoltHelpers.Time
{
    public class ContinousTimeCollection<Tag>
    {
        private bool _requireSort;
        private List<Time.SingleTime<Tag>> _times;
        private List<Time.SingleTime<Tag>> _sortedList;
        public List<Time.SingleTime<Tag>> SortedTimes
        {
            get
            {
                lock (_times)
                {
                    if (_requireSort)
                    {
                        _requireSort = false;
                        return (_sortedList = _times.OrderBy(x => x.TimeStart).ToList());
                    }

                    return _sortedList;
                }
            }
        }


        public ContinousTimeCollection()
        {
            _times = new List<SingleTime<Tag>>();
        }

        public void Add(Time.SingleTime<Tag> item)
        {
            lock (_times)
            {
                _times.Add(item);
                _requireSort = true;
            }
        }

        public void AddRange(IEnumerable<Time.SingleTime<Tag>> items)
        {
            lock (_times)
            {
                _times.AddRange(items);
                _requireSort = true;
            }
        }

        public bool IsEmpty()
        {
            lock (_times)
            {
                return _times.Count == 0;
            }
        }
    }
}
