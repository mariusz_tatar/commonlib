﻿# VoltHelpers

Biblioteka która udostepnia różne pomocne funkcje pomocne

## Instalacja

Aby zainstalować bibliotekę należy pobrać skompilowaną DLL lub pobrać ją z firmowgo NuGet-a spod adresu [NugetServer96Volt](http://nugetserver.96volt.com/nuget) pod nazwą 'VoltHelpers'

## Użycie

```csharp

```

## Modyfikacja

Po modyfikacji biblioteki należy zmienić numer wersji w pliku ''VoltHelpers.nuspec'' oraz wykonać skrypt ''Publikowanie pakietów nuget.ps1''


