﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Time
{
    /// <summary>
    /// Testy pomocy.
    /// Aby móc wyświetlać obrazny opisu funckji należy zainstalować rozszerzenie do VS: https://marketplace.visualstudio.com/items?itemName=MariusBancila.memefulcomments
    /// Plik dla VS2019 jest równiez w aktualnym katalogu
    /// </summary>
    [TestClass]
    public class TimeCommonRange
    {
        private class Range : VoltHelpers.Time.TimeRange<int>
        {
            public Range(DateTime timeStart, DateTime timesEnd)
                : base(timeStart, timesEnd)
            {
            }
        }

        [TestMethod]
        public void NoRanges_NoReturn()
        {
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(Enumerable.Empty<Range>(), Enumerable.Empty<Range>());

            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void OneSlaveNoMaster_NoReturn()
        {
            var range1 = new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10));
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(new[] { range1 }, Enumerable.Empty<Range>());

            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void OneMasterNoSlave_NoReturn()
        {
            var range1 = new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10));
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(Enumerable.Empty<Range>(), new[] { range1 });

            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        [TestMethod]
        /// <image url="$(ProjectDir)Time\images\1_slave_1_master_in.png" scale="0.7" />
        public void OneSlaveOneMaster_OneResult()
        {
            var rangeSlave = new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10));
            var rangeMaster = new Range(new DateTime(2000, 1, 2), new DateTime(2000, 1, 9));
            var expectedRange = (Range)rangeSlave.Clone();
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(new[] { rangeSlave }, new[] { rangeMaster }).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(new[] { rangeMaster });
        }

        [TestMethod]
        /// <image url="$(ProjectDir)Time\images\1_slave_2_master.png" scale="0.7" />
        public void OneSlaveInOneMasterAndMaster_OneResult()
        {
            var masterRanges = new[]
              {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 10), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10))
            };

            var expected = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10))
            };

            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\1_slave_2_master_in.png" scale="0.7" />
        [TestMethod]
        public void OneSlaveOver2Masters()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 2), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 15), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 30))
            };

            var expected = masterRanges;
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\1_slave_2_master_in_the_same_time.png" scale="0.7" />
        [TestMethod]
        public void OneSlaveOver2MasterTheSameTime()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 15), new DateTime(2000, 1, 30))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 30))
            };

            var expected = masterRanges;

            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\1_slave_in_1_master.png" scale="0.7" />
        [TestMethod]
        public void OneSlaveInOneMaster()
        {
            var masterRanges = new[]
           {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 5), new DateTime(2000, 1, 8))
            };

            var expected = slaveRanges;
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\2_slave_1_master.png" scale="0.7" />
        [TestMethod]
        public void TwoSlaveInOneMaster()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 2), new DateTime(2000, 1,10)),
                 new Range(new DateTime(2000, 1, 15), new DateTime(2000, 1, 19))
            };

            var expected = slaveRanges;
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\2_slave_1_master_in_the_same_time.png" scale="0.7" />
        [TestMethod]
        public void TwoSlaveInOneMasterTheSameTime()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1,10)),
                 new Range(new DateTime(2000, 1, 10), new DateTime(2000, 1, 20))
            };

            var expected = slaveRanges;
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\2_slave_2_master.png" scale="0.7" />
        [TestMethod]
        public void TwoSlaveTwoMasterDifferentTimes()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 5)),
                new Range(new DateTime(2000, 1, 20), new DateTime(2000, 1, 30))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 6), new DateTime(2000, 1,7)),
                 new Range(new DateTime(2000, 1, 8), new DateTime(2000, 1, 15))
            };

            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\2_slave_2_master_in_the_same_time.png" scale="0.7" />
        [TestMethod]
        public void TwoSlaveTwoMasterTheSameTime()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 10), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
               new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 10), new DateTime(2000, 1, 20))
            };

            var expected = slaveRanges;
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\3_slave_1_master.png" scale="0.7" />
        [TestMethod]
        public void ThreeSlavesOneMaster()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 5), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 6)),
                new Range(new DateTime(2000, 1, 7), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 12), new DateTime(2000, 1, 30))
            };

            var expected = new[]
            {
                new Range(new DateTime(2000, 1, 5), new DateTime(2000, 1, 6)),
                new Range(new DateTime(2000, 1, 7), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 12), new DateTime(2000, 1, 20))
            };
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\3_slave_1_master_before.png" scale="0.7" />
        [TestMethod]
        public void ThreeSlavesOneMaster2()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 20))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 2), new DateTime(2000, 1, 6)),
                new Range(new DateTime(2000, 1, 7), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 12), new DateTime(2000, 1, 30))
            };

            var expected = new[]
            {
                new Range(new DateTime(2000, 1, 2), new DateTime(2000, 1, 6)),
                new Range(new DateTime(2000, 1, 7), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 12), new DateTime(2000, 1, 20))
            };
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\3_slave_1_master_in_one_slave.png" scale="0.7" />
        [TestMethod]
        public void ThreeSlavesOneMasterInOneSlave()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 8), new DateTime(2000, 1, 9))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 2), new DateTime(2000, 1, 6)),
                new Range(new DateTime(2000, 1, 7), new DateTime(2000, 1, 10)),
                new Range(new DateTime(2000, 1, 12), new DateTime(2000, 1, 30))
            };

            var expected = new[]
            {
                new Range(new DateTime(2000, 1, 8), new DateTime(2000, 1, 9))
            };
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\1_slave_after_master.png" scale="0.7" />
        [TestMethod]
        public void OneSlaveOneMasterDifferentTime()
        {
            var rangeSlave = new Range(new DateTime(2000, 10, 1), new DateTime(2000, 10, 10));
            var rangeMaster1 = new Range(new DateTime(2000, 1, 1), new DateTime(2000, 1, 10));
            var rangeMaster2 = new Range(new DateTime(2000, 1, 10), new DateTime(2000, 1, 15));

            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(new[] { rangeSlave }, new[] { rangeMaster1, rangeMaster2 }).ToList();

            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\many_slaves_one_master.png" scale="0.7" />
        [TestMethod]
        public void ManySlavesOneMasterTimeOfExecutionMeasures()
        {
            var rangeSlave = Enumerable.Range(1, 100000).Select(x => new Range(new DateTime(2000, 1, 1).AddDays(x).AddHours(8), new DateTime(2000, 1, 1).AddDays(x).AddHours(16)));
            var masterRange = new[] { new Range(new DateTime(2000, 1, 1), new DateTime(2500, 1, 1)) };
            var expected = rangeSlave;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(rangeSlave, masterRange).ToList();
            sw.Stop();
            Console.WriteLine("Execute time [ms]: " + sw.ElapsedMilliseconds);

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\many_slaves_one_master.png" scale="0.7" />
        [TestMethod]
        public void ManySlavesManyMasters()
        {
            var rangeSlave = Enumerable.Range(1, 10000).Select(x => new Range(new DateTime(2000, 1, 1).AddDays(x).AddHours(8), new DateTime(2000, 1, 1).AddDays(x).AddHours(16)));
            var masterRange = Enumerable.Range(1, 1000).Select(x => new Range(new DateTime(2000, 1, 1).AddDays(x).AddHours(8), new DateTime(2000, 1, 1).AddDays(x).AddHours(16)));
            var expected = rangeSlave;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(rangeSlave, masterRange).ToList();
            sw.Stop();
            Console.WriteLine("Execute time [ms]: " + sw.ElapsedMilliseconds);

            result.Should().NotBeNull();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\3_slave_1_master.png" scale="0.7" />
        [TestMethod]
        public void OneMasterThreeSlaveWithHours()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1,07,0,0), new DateTime(2000, 1, 1,22,0,0))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1,6,0,0), new DateTime(2000, 1, 1,8,1,15)),
                new Range(new DateTime(2000, 1, 1,12,4,6), new DateTime(2000, 1, 1,13,57,6)),
                new Range(new DateTime(2000, 1, 1,9,15,4), new DateTime(2000, 1, 1,23,15,47))
            };

            var expected = new[]
            {
                new Range(new DateTime(2000, 1, 1,07,0,0),new DateTime(2000, 1, 1,8,1,15)),
                new Range(new DateTime(2000, 1, 1,12,4,6), new DateTime(2000, 1, 1,13,57,6)),
                new Range(new DateTime(2000, 1, 1,9,15,4), new DateTime(2000, 1, 1,22,0,0))
            };
            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <image url="$(ProjectDir)Time\images\2_slave_3_masters_same_time.png" scale="0.7" />
        [TestMethod]
        public void TwoSlavesThreeMastersSameTime()
        {
            var masterRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1,07,0,0), new DateTime(2000, 1, 1,12,0,0)),
                new Range(new DateTime(2000, 1, 1,15,0,0), new DateTime(2000, 1, 1,18,0,0)),
                new Range(new DateTime(2000, 1, 1,18,0,0), new DateTime(2000, 1, 1,22,0,0))
            };

            var slaveRanges = new[]
            {
                new Range(new DateTime(2000, 1, 1,12,0,0), new DateTime(2000, 1, 1,15,0,0)),
                new Range(new DateTime(2000, 1, 1,16,4,6), new DateTime(2000, 1, 1,22,0,0))
            };

            var expected = new[]
            {
                new Range(new DateTime(2000, 1, 1,16,4,6), new DateTime(2000, 1, 1,18,0,0)),
                new Range( new DateTime(2000, 1, 1,18,0,0), new DateTime(2000, 1, 1,22,0,0))
            };

            var result = VoltHelpers.Time.TimeCommonRange.CalculateCommonRanges(slaveRanges, masterRanges).ToList();

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expected);
        }

    }
}
