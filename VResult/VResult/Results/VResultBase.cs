﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using VResult.Messages;
using VResult.Messages.Localizable;
using VResult.Reasons;

namespace VResult.Results
{
    public abstract class VResultBase
    {
        public List<Reasons.Reason> Reasons { get; private set; }
        public ReadOnlyCollection<Info> Infos { get { return Reasons.OfType<Info>().ToList().AsReadOnly(); } }
        public ReadOnlyCollection<Error> Errors { get { return Reasons.OfType<Error>().ToList().AsReadOnly(); } }
        public ReadOnlyCollection<Warning> Warnings { get { return Reasons.OfType<Warning>().ToList().AsReadOnly(); } }

        public bool IsSuccess { get { return !IsFailed; } }
        public bool IsFailed { get { return Reasons.OfType<Error>().Any(); } }




        public VResultBase()
        {
            Reasons = new List<Reason>();
        }

    }
}
