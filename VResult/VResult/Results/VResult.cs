﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VResult.Messages;
using VResult.Messages.Localizable;

namespace VResult.Results
{
    public class VResult : VResultBase
    {

        public VResult()
            : base()
        {
        }   

        public VResult WithWarning(Reasons.Warning warning)
        {
            base.Reasons.Add(warning);
            return this;
        }

        public VResult WithInfo(Reasons.Info info)
        {
            base.Reasons.Add(info);
            return this;
        }

        public VResult WithError(Reasons.Error error)
        {
            base.Reasons.Add(error);
            return this;
        }
    }
}
