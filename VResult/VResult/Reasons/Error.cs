﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VResult.Messages;

namespace VResult.Reasons
{
    public class Error : Reason
    {
        public int ErrorCode { get; set; }
        public List<Error> Errors { get; private set; }



        public Error()
            : base()
        {
            Errors = new List<Error>();
        }

        public Error(MessageBase message)
            : base(message)
        {
            Errors = new List<Error>();
        }

        public Error(MessageBase message, int errorCode)
          : base(message)
        {
            ErrorCode = errorCode;
            Errors = new List<Error>();
        }

        public Error WithMessage(MessageBase message)
        {
            base.Message = message;
            return this;
        }


        public Error WithException(MessageBase message, Exception exception)
        {
            this.Message = message;
            Errors.Add(new Reasons.ExceptionError(exception));
            return this;
        }

        public Error WithErrorCode(int errorCode)
        {
            ErrorCode = errorCode;
            return this;
        }

        public Error WithMetadata(string metadataName, object metadataValue)
        {
            Metadata.Add(metadataName, metadataValue);
            return this;
        }
    }
}
