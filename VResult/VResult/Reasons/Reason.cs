﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VResult.Reasons
{
   public class Reason
    {
        public Messages.MessageBase Message { get; set; }
        public Dictionary<string, object> Metadata { get; protected set; }


        public Reason()
        {
            Metadata = new Dictionary<string, object>();
        }

        public Reason(Messages.MessageBase message)
        {
            Message = message;
            Metadata = new Dictionary<string, object>();
        }
    }
}
