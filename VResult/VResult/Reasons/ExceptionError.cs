﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VResult.Messages;

namespace VResult.Reasons
{
    public class ExceptionError : Error
    {
        public Exception Exception { get; set; }


        public ExceptionError(Exception exception)
        {
            Exception = exception;
        }

        public ExceptionError(MessageBase message,Exception exception) 
            : base(message)
        {
            this.Exception = exception;
        }
    }
}
