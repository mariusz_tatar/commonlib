﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VResult.Messages;

namespace VResult.Reasons
{
  public  class Info:Reason
    {
        public Info()
        {
        }

        public Info(MessageBase message)
            : base(message)
        {
        }

        public Info WithMetadata(string metadataName, object metadataValue)
        {
            Metadata.Add(metadataName, metadataValue);
            return this;
        }
    }
}
