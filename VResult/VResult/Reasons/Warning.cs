﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VResult.Messages;

namespace VResult.Reasons
{
    public class Warning : Reason
    {
        public Warning()
        {
        }

        public Warning(MessageBase message) 
            : base(message)
        {
        }

        public Warning WithMetadata(string metadataName, object metadataValue)
        {
            Metadata.Add(metadataName, metadataValue);
            return this;
        }
    }
}
