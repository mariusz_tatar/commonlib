﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace VResult.Messages.Localizable
{
    public interface ILocalizable
    {
        CultureInfo GetCurrentCulture();
    }
}
