﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VResult.Messages
{
    public class SimpleLangMessage : MessageBase
    {
        public string PL { get; set; }
        public string EN { get; set; }


        public SimpleLangMessage(string pl, string en)
        {
            PL = pl;
            EN = en;
        }

        public override string GetMessageString()
        {
            throw new NotImplementedException();
        }
    }
}
