﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VResult.Messages
{
    public abstract class MessageBase
    {
        public abstract string GetMessageString();
    }
}
