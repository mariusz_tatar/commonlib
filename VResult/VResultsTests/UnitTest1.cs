﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VResult.Messages;

namespace VResultsTests
{
    [TestClass]
    public class VResultsTests
    {
        [TestMethod]
        public void NoRanges_NoReturn()
        {
            var r = new VResult.Results.VResult();
            var messageError = new VResult.Reasons.Error(new VResult.Messages.SimpleLangMessage("błąd", "")).WithErrorCode(1);
            r.WithError(messageError);

            //asssert
            r.Infos.Should().BeEmpty();
            r.IsFailed.Should().BeTrue();
            r.Warnings.Should().BeEmpty();
            r.Errors.Should().BeEquivalentTo(new[] { messageError });
            r.Errors.Single().ErrorCode.Should().Be(1);
            (r.Errors.Single().Message as SimpleLangMessage).PL.Should().Be("błąd");

        }
    }
}
