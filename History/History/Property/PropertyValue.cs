﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.Property
{
    public class PropertyValue
    {
        public string PropertyName { get; set; }
        public string Value { get; set; }

        public PropertyValue(string propertyName, string value)
        {
            PropertyName = propertyName;
            Value = value;
        }
    }
}
