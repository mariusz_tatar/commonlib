﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.Property
{
    public class PropertyCollection<TRecord>
    {
        public readonly TRecord Record;
        private readonly SqlConnection sqlConnection;

        public List<PropertyInfoBase<TRecord>> PropertyList { get; private set; }


        public PropertyCollection(TRecord record, SqlConnection sqlConnection)
        {
            this.Record = record;
            this.sqlConnection = sqlConnection;
            PropertyList = new List<PropertyInfoBase<TRecord>>();
        }

        public PropertyCollection<TRecord> AddProperty(PropertyInfoBase<TRecord> propertyInformation)
        {
            PropertyList.Add(propertyInformation);
            return this;
        }
       
        public IList<PropertyValue> GenerateChanges()
        {
            var returnList = new List<PropertyValue>();

            foreach (var item in PropertyList)
                returnList.Add(item.GetChange(sqlConnection, Record));

            return returnList;
        }
    }
}
