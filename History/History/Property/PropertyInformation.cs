﻿using NGVolt.History.NameProviders;
using NGVolt.History.ValueProviders;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.Property
{
    public enum BehaviorAlwaysAdd
    {
        /// <summary>
        /// Dodaje tylko jeśli jest zmiana
        /// </summary>
        DefaultOnlyIfChange,

        /// <summary>
        /// Zawsze dodaje ale tylko jeśli są inne zmiany nie oznaczone "Allways" albo w aktualnym elemencie jest zmiana
        /// </summary>
        AlwaysAddIfChangeOrOtherDefaultChanges
    }

    public enum SourceOdChange
    {
        FromChange,
        FromAlwaysAdd
    }

    public abstract class PropertyInfoBase<TRecord>
    {
        public BehaviorAlwaysAdd IsAlwaysAdd { get; protected set; }
        public abstract IComparable GetRawValue(TRecord record);
        public abstract PropertyValue GetChange(SqlConnection sqlConnection, TRecord record);
    }

    public class PropertyInformation<TRecord, TValue> : PropertyInfoBase<TRecord> where TValue : IComparable
    {
        private string _name;
        private INameProvider _nameProvider;
        private IValueProvider<TRecord, TValue> _valueProvider;
        private readonly Func<TRecord, TValue> _valueSelector;
        private Action<PropertyInformation<TRecord, TValue>> _configAction;
        private Func<TRecord, TValue, string> _valueCompute;







        public PropertyInformation(Func<TRecord, TValue> valueSelector)
            : this(valueSelector, BehaviorAlwaysAdd.DefaultOnlyIfChange)
        {
        }

        public PropertyInformation(Func<TRecord, TValue> valueSelector, BehaviorAlwaysAdd isAlwaysAdd)
        {
            _valueSelector = valueSelector;
            IsAlwaysAdd = isAlwaysAdd;
        }

        public PropertyInformation<TRecord, TValue> WithName(string name)
        {
            _name = name;
            return this;
        }

        public PropertyInformation<TRecord, TValue> WithNameByProvider(INameProvider nameProvider)
        {
            _nameProvider = nameProvider;
            return this;
        }

        public PropertyInformation<TRecord, TValue> WithValue(Func<TRecord, TValue, string> valueCompute)
        {
            _valueCompute = valueCompute;
            return this;
        }

        public PropertyInformation<TRecord, TValue> WithValueByProvider(IValueProvider<TRecord, TValue> valueProvider)
        {
            _valueProvider = valueProvider;
            return this;
        }

        public PropertyInformation<TRecord, TValue> Config(Action<PropertyInformation<TRecord, TValue>> action)
        {
            _configAction = action;
            return this;
        }

        public override PropertyValue GetChange(SqlConnection sqlConnection, TRecord record)
        {
            if (_configAction != null)
                _configAction(this);

            return new PropertyValue(GetName(sqlConnection, record), GetValue(sqlConnection, record));
        }

        private string GetValue(SqlConnection sqlConnection, TRecord record)
        {
            if (_valueCompute != null)
                return _valueCompute(record, _valueSelector(record));

            if (_valueProvider != null)
                return _valueProvider.GetValue(_valueSelector(record));

            return "";
        }

        private string GetName(SqlConnection sqlConnection, TRecord record)
        {
            if (_name != null)
                return _name;

            if (_nameProvider != null)
                return _nameProvider.GetName();

            return "";
        }

        public static string GetMemberNamFromBody(Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("");
            }

            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression = (MemberExpression)expression;
                return memberExpression.Member.Name;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression = (MethodCallExpression)expression;
                return methodCallExpression.Method.Name;
            }

            if (expression is UnaryExpression)
            {
                // Property, field of method returning value type
                var unaryExpression = (UnaryExpression)expression;
                return GetMemberName(unaryExpression);
            }

            throw new ArgumentException("");
        }

        private static string GetMemberName(UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression)
            {
                var methodExpression = (MethodCallExpression)unaryExpression.Operand;
                return methodExpression.Method.Name;
            }

            return ((MemberExpression)unaryExpression.Operand).Member.Name;
        }

        public override IComparable GetRawValue(TRecord record)
        {
            return _valueSelector(record);
        }

    }
}
