﻿using NGVolt.History.Property;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.Interfaces
{
    public interface IHistoryObject<TRecord>
    {
        PropertyCollection<TRecord> GetPropertiesDescription(SqlConnection connection);
    }
}
