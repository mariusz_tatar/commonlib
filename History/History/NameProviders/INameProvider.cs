﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.NameProviders
{
  public  interface INameProvider
    {
        string GetName();
    }
}
