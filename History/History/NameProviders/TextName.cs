﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.NameProviders
{
    public class TextName : INameProvider
    {
        private readonly string name;

        public TextName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }
    }
}
