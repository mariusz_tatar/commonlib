﻿using NGVolt.History.Interfaces;
using NGVolt.History.Property;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NGVolt.History
{
    public class HistoryManager
    {

        public HistoryManager()
        {
        }


        public IList<Change> GetAllChangesOfRecord<TRecord>(SqlConnection sqlConnection, IHistoryObject<TRecord> recordBeforeSave, IHistoryObject<TRecord> recordAfterSave)
        {
            var listOfChanges = new List<Change>();

            if (recordBeforeSave == null && recordAfterSave == null)
                return listOfChanges;

            if (recordBeforeSave == null)
            {
                //dodanie zmian wynikających z nowego rekordu tylko
                FillListOfValues(sqlConnection, recordAfterSave.GetPropertiesDescription(sqlConnection), listOfChanges, x => new Change(new PropertyValue(x.PropertyName, ""), x, SourceOdChange.FromChange));
            }
            else if (recordAfterSave == null)
            {
                //dodanie zmian wynikających z 
                FillListOfValues(sqlConnection, recordBeforeSave.GetPropertiesDescription(sqlConnection), listOfChanges, x => new Change(x, new PropertyValue(x.PropertyName, ""), SourceOdChange.FromChange));
            }
            else
            {
                var before = recordBeforeSave.GetPropertiesDescription(sqlConnection);
                var after = recordAfterSave.GetPropertiesDescription(sqlConnection);
                var toRemoweIfNotExistsOtherChanges = new List<Change>();
                bool existsDefaultChanges = false;

                for (int i = 0; i < before.PropertyList.Count; i++)
                {
                    var propertyBefore = before.PropertyList[i];
                    var propertyAfter = after.PropertyList[i];
                    bool isDifferentValues = false;
                    bool checkValues = false;
                    bool addChange = false;

                    switch (propertyBefore.IsAlwaysAdd)
                    {
                        case BehaviorAlwaysAdd.DefaultOnlyIfChange:
                            checkValues = true;
                            break;
                        case BehaviorAlwaysAdd.AlwaysAddIfChangeOrOtherDefaultChanges:
                            checkValues = true;
                            addChange = true;
                            break;
                    }

                    if (checkValues)
                    {
                        var valueBefore = propertyBefore.GetRawValue(before.Record);
                        var valueAfter = propertyAfter.GetRawValue(after.Record);
                        isDifferentValues = valueBefore.CompareTo(valueAfter) != 0;
                    }

                    if (addChange || isDifferentValues)
                    {
                        SourceOdChange sourceOdChange = isDifferentValues ? SourceOdChange.FromChange : SourceOdChange.FromAlwaysAdd;
                        var propertyChangeObject = new Change(propertyBefore.GetChange(sqlConnection, before.Record), propertyAfter.GetChange(sqlConnection, after.Record), sourceOdChange);
                        listOfChanges.Add(propertyChangeObject);

                        if (sourceOdChange == SourceOdChange.FromChange)
                            existsDefaultChanges = true;
                    }
                }

                if (existsDefaultChanges == false)
                    listOfChanges.Clear();
            }

            return listOfChanges;
        }

        private void FillListOfValues<TRecord>(SqlConnection sqlConnection, PropertyCollection<TRecord> propertyCollection, List<Change> listToFill, Func<PropertyValue, Change> createChangeObject)
        {
            foreach (var propertyInformation in propertyCollection.PropertyList)
            {
                var valueAfter = propertyInformation.GetChange(sqlConnection, propertyCollection.Record);
                listToFill.Add(createChangeObject(valueAfter));
            }
        }
    }
}
