﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.ValueProviders
{
    public enum FastFormat
    {
        DateOnly,
        TimeOnly,
        DateTime,
        Custom
    }

    public class DateTimeProvider<TRecord> : IValueProvider<TRecord, DateTime>
    {
        private readonly FastFormat fastFormat;
        private readonly string format;



        public DateTimeProvider(FastFormat fastFormat, string format)
        {
            this.fastFormat = fastFormat;
            this.format = format;
        }

        public DateTimeProvider(FastFormat fastFormat)
            : this(fastFormat, "")
        {
        }


        public string GetValue(DateTime value)
        {
            switch (fastFormat)
            {
                case FastFormat.DateOnly: return value.ToString("yyyy-MM-dd");
                case FastFormat.TimeOnly: return value.ToString("HH:mm:ss");
                case FastFormat.DateTime: return value.ToString("yyyy-MM-dd HH:mm:ss");
                case FastFormat.Custom: return value.ToString(format);
            }

            return value.ToLongDateString();
        }
    }
}
