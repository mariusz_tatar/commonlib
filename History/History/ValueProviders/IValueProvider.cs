﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.ValueProviders
{
    public interface IValueProvider<TRecord,TValue>
    {
        string GetValue(TValue value);
    }
}
