﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.History.ValueProviders
{
    public class BooleanProvider<TRecord, TValue> : IValueProvider<TRecord, TValue> where TValue : struct, IConvertible
    {
        private readonly string trueLabel;
        private readonly string falseLabel;

        public BooleanProvider(string trueLabel, string falseLabel)
        {
            this.trueLabel = trueLabel;
            this.falseLabel = falseLabel;
        }

        public BooleanProvider()
            : this("TAK", "NIE")
        {
        }


        public string GetValue(TValue value)
        {
            if (Convert.ToBoolean(value))
                return trueLabel;

            return falseLabel;
        }
    }
}
