﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Text;
//using System.Threading.Tasks;

//namespace NGVolt.History.ValueProviders
//{
//    public class ForeinIntKeyProvider<TForeinRecord, TRecord> : IValueProvider<TRecord, int>
//    {
//        private readonly string foreinTableName;
//        private readonly string localTableName;
//        private readonly Expression<Func<TRecord, object>> localProperty;
//        private readonly Expression<Func<TForeinRecord, object>> foreinProperty;
//        private Expression<Func<TForeinRecord, object>>[] columnsToRead;
//        private Func<TForeinRecord, string> computeFunction;



//        public ForeinIntKeyProvider(string foreinTableName, string localTableName, Expression<Func<TForeinRecord, object>> foreinProperty, Expression<Func<TRecord, object>> localProperty)
//        {
//            this.foreinTableName = foreinTableName;
//            this.localTableName = localTableName;
//            this.foreinProperty = foreinProperty;
//            this.localProperty = localProperty;

//            if (foreinProperty == null)
//                throw new Exception("Wartośc 'foreinProperty' nie może być pusta");

//            if (localProperty == null)
//                throw new Exception("Wartośc 'localProperty' nie może być pusta");

//            if (foreinTableName == null)
//                throw new Exception("Wartośc 'foreinTableName' nie może być pusta");
//        }

//        public ForeinIntKeyProvider<TForeinRecord, TRecord> SetColumnToReadAndCopute(Expression<Func<TForeinRecord, object>>[] columnsToRead, Func<TForeinRecord, string> computeFunction)
//        {
//            this.columnsToRead = columnsToRead;
//            this.computeFunction = computeFunction;
//            return this;
//        }



//        public string GetValue(int value)
//        {
//            if (columnsToRead == null || columnsToRead.Any() == false)
//                return "";

//            string foreinKeyColumnName = GetMemberName(foreinProperty);
//            string localPropertyName = GetMemberName(localProperty);
//            string columnsNamesToRead = string.Join(",", columnsToRead.Select(x => GetMemberName(x)));
//            string sql = string.Format(@"SELECT {0} FROM {1} as _from
//                                        JOIN {3} _join on _join
//                                        WHERE ", columnsNamesToRead
//            return value.ToString();
//        }

//        public static string GetMemberName<T>(Expression<Func<T, object>> expression)
//        {
//            return GetMemberNamFromBody(expression.Body);
//        }


//        public static string GetMemberNamFromBody(Expression expression)
//        {
//            if (expression == null)
//            {
//                throw new ArgumentException("Błędne wskazanie pola");
//            }

//            if (expression is MemberExpression)
//            {
//                // Reference type property or field
//                var memberExpression = (MemberExpression)expression;
//                return memberExpression.Member.Name;
//            }

//            if (expression is MethodCallExpression)
//            {
//                // Reference type method
//                var methodCallExpression = (MethodCallExpression)expression;
//                return methodCallExpression.Method.Name;
//            }

//            if (expression is UnaryExpression)
//            {
//                // Property, field of method returning value type
//                var unaryExpression = (UnaryExpression)expression;
//                return GetMemberName(unaryExpression);
//            }

//            throw new ArgumentException("Błędne wskazanie pola");
//        }

//        private static string GetMemberName(UnaryExpression unaryExpression)
//        {
//            if (unaryExpression.Operand is MethodCallExpression)
//            {
//                var methodExpression = (MethodCallExpression)unaryExpression.Operand;
//                return methodExpression.Method.Name;
//            }

//            return ((MemberExpression)unaryExpression.Operand).Member.Name;
//        }
//    }
//}