﻿using NGVolt.History.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGVolt.History
{
    public  class Change
    {
        public readonly PropertyValue ValueBefore;
        public readonly PropertyValue ValueAfter;
        public readonly SourceOdChange SourceOfItem;


        public Change(PropertyValue valueBefore, PropertyValue valueAfter, SourceOdChange sourceOfItem)
        {
            ValueBefore = valueBefore;
            ValueAfter = valueAfter;
            SourceOfItem = sourceOfItem;

            if (valueAfter.PropertyName.Equals(valueBefore.PropertyName) == false)
                throw new Exception(string.Format("Nazwy pól zmiany muszą być identyczne: ValueBefore={0} ValueAfter={1}", valueBefore.PropertyName, valueAfter.PropertyName));
        }
    }
}
