﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        public class RozbicieGodzin
        {
            public int Id = -1;
            public int IdKartoteka = -1;
            public int Pochodzenie = 1;
            public DateTime Data = DateTime.MinValue;
            public DateTime GodzinaOd = DateTime.MinValue;
            public DateTime GodzinaDo = DateTime.MinValue;
            public int LiczbaMinut = 0;
            public int Pochodzenie_IdKalkulacje = -1;
            public int Pochodzenie_IdWniosek = -1;
            public int Pochodzenie_IdKalkulacje_TypSkladnika = -1;

        }

        public class GodzinyPracy
        {
            public int Id;
            public DateTime DataEfektywna;
            public DateTime DzienCzasPracyStart;
            public DateTime NocCzasPracyStart;
        }
        public enum TypDnia
        {
            roboczy
        }
        public class Record
        {
            public int Id = -1;
            public int KartotekaId = -1;
            public DateTime Data = DateTime.Now;
            public TypDnia TypDnia = TypDnia.roboczy;
            public int Norma = 0;
            public DateTime ZarejestrowanaGodzinaWejscia = DateTime.MinValue;
            public DateTime ZarejestrowanaGodzinaWyjscia = DateTime.MinValue;
            public DateTime ZaliczonaGodzinaWejscia = DateTime.MinValue;
            public DateTime ZaliczonaGodzinaWyjscia = DateTime.MinValue;
            public int CzasPracy = 0;
            public int CzasPracyZaliczonyDzien = 0;
            public int CzasPracyZaliczonyNoc = 0;
            public int Spoznienie = 0;
            public int WyjsciePrywatne = 0;

            public int WyjściaSluzbowe = 0;


            public int NadgodzinyDzien = 0;


            public int NadgodzinyNoc = 0;


            public int IdrekorduZrodlowego = -1;


            public int DodanieKartotekaId = -1;


            public DateTime DodanieData = DateTime.MinValue;


            public int ModyfikacjaKartotekaId = -1;


            public DateTime ModyfikacjaData = DateTime.MinValue;


            public int SchematUzytyDoObliczen = -1;

            public string DodatkoweInformacje = string.Empty;


            public DateTime DataZakonczeniaDomyRozliczeniowej = DateTime.MinValue;


            public int ZaliczoneNadgodzinyDzien = 0;


            public int ZaliczoneNadgodzinyNoc = 0;


            public DateTime ZarejestrowanaOrginalnaGodzinaWejscia = DateTime.MinValue;


            public DateTime ZarejestrowanaOrginalnaGodzinaWyjscia = DateTime.MinValue;


            public int CzasUrlopuGodzinowego = 0;


            public int CzasPracyZaliczonyNominalnyDzien = 0;


            public int CzasPracyZaliczonyNominalnyNoc = 0;


            public int GodzinyOdebrane = 0;


            public int ZaliczonyCzasOdpracowaniaDzien = 0;


            public int ZaliczonyCzasOdpracowaniaNoc = 0;

            public string PowodZmiany = "";


            public int DelegacjaZaliczonyCzas = 0;


            public int PracaZdalnaZaliczonyCzas = 0;


            public int LiczbaMinutPonadwymiarowychMiedzyNormaA8h = 0;


            public int NadgodzinyBezPozwolen = 0;


            public int CzasPracyWDniWolneDoNormyBezPozwolen = 0;


            public int TypDniaKalendarz = 0;

            /// <summary>
            /// Godzina od której zaczyna się pierwsza minuta nadgodzin dziennych 
            /// Docelowo kiedyś trzeba przepisać tak żeby to była dodatkowa informacja w osobnej tabeli
            /// </summary>

            public DateTime GodzRozpoczeciaZaliczonychNadgodzinDzien = DateTime.MinValue;

            /// <summary>
            /// Godzina od której zaczyna się pierwsza minuta nadgodzin nocnych 
            /// Docelowo kiedyś trzeba przepisać tak żeby to była dodatkowa informacja w osobnej tabeli
            /// </summary>

            public DateTime GodzRozpoczeciaZaliczonychNadgodzinNoc = DateTime.MinValue;

            /// <summary>
            /// Liczba minut godzin ponadwymiarowych niezaliczonych z powodu braku pozwolenia na godziny ponadwymiarowe
            /// </summary>

            public int LiczbaMinutPonadwymiarowychMiedzyNormaA8hNieZaliczonych = 0;


            /// <summary>
            /// Czas wyjścia który wynika z rzeczywiście przepracowanych i zaliczonych godzin
            /// </summary>

            public DateTime ZaliczonaGodzinaWyjsciaWgZaliczonegoCzasuPracy = DateTime.MinValue;



            public int CzasUrlopuGodzinowegoNiewliczanegoDoCzasuPracy = 0;

            public bool ObliczanieRowniezZDniaWczesniejszego = false;

            public bool RekordWygenerowanyAutomatycznieTylkoDoRecznejEdycji = false;



            public int CzasUrlopuGodzinowegoNiewliczanegoDoCzasuPracyAleZMozliwosciaOdpracowania = 0;


            public int RzeczywistyCzasPracy = 0;

            public bool JestNieobecnoscCalodniowa;


            public int LiczbaMinutPonadwymiarowychMiedzyNormaA8hDzienne = 0;


            public int LiczbaMinutPonadwymiarowychMiedzyNormaA8hNocne = 0;


            public int GodzinyPonadwymiaroweKolejnosc = 0;


            public int GodzinyOdebraneWTymPonadwymiarowe = 0;


            public int CzasZaliczonyDoCzasuPracyPrzedGodzRozpoczecia = 0;


            public int CzasZaliczonyDoCzasuPracyPoGodzZakonczenia = 0;


            public DateTime DataRozpoczeciaDobyRozliczeniowej = DateTime.MinValue;
        }

        public class SkladnikCzasu
        {
            public int Id = -1;
            public int IdKalkulacjaUnis;
            public int Typ = 0;
            public int Rodzaj = 0;
            public DateTime GodzinaOd;
            public DateTime GodzinaDo;
            public int LiczbaMinut;
        }

        public class KartotekiPersonalne
        {

            public int Id = -1;
            public int Id_Manager = -1;
            public int Id_TeamLeader = -1;
            public int Id_PreferowanyZastepca = -1;
            public int EwidencjonujeCzasPracy = 0;
            public int SposobZalozeniaKartoteki = 0;
            public int EksportDanych = 0;
            public int ZmieniajDanePodczasImportu = 1;
            public int PreferowanyJezyk = 0;
            public int Nieobecny = 0;
            public int Aktywny = 1;
            public int MozliwoscWprowadzeniaNadgodzin = 1;
            public DateTime DataZatrudnienia = DateTime.MinValue;
            public DateTime DataZakonczenia = DateTime.MinValue;
            public DateTime DataZalozenia = DateTime.MinValue;
            public DateTime DataAktualizacji = DateTime.MinValue;
            public DateTime PreferowanyCzasRozpoczeciaPracy = DateTime.MinValue;
            public string NumerPlacowy = string.Empty;
            public string Identyfikator = string.Empty;
            public string Haslo = string.Empty;
            public string Imie = string.Empty;
            public string Nazwisko = string.Empty;
            public string Email = string.Empty;
            public int Id_Stanowisko = -1;
            public int Id_OpCo = -1;
            public int Id_Proces = -1;
            public int Id_Zespol = -1;
            public int Id_Departament = -1;
            public int Id_CentrumKosztowe = -1;
            public DateTime PreferowanyCzasRozpoczeciaPrzerwy = DateTime.MinValue;
            public DateTime PreferowanyCzasTrwaniaPrzerwy = DateTime.MinValue;
            public int PokazujPracownikowBezposrednich = 0;
            public string TypeOfContract = string.Empty;
            public DateTime OstatniaZmianaHasla = DateTime.Now;
            public string Telefon = string.Empty;
            public int IdAutoId = -1;
            public string IdentyfikatorUnis = string.Empty;
            public string NrDokumentu = string.Empty;
            public int Id_GrupaDostepu = -1;
            public DateTime DataModyfikacjiUNIS = DateTime.MinValue;
            public string UwagiDoPracownika = string.Empty;
            public int WymuszajZmianeHasla = 0;
            public int ZmieniajDanePodczasImportuParametrow = 0;
            public int MetodaKalkulacji = 0;
            public int PreferowanePodpowiedziGodzinObecnosci = 0;
            public DateTime PreferowanaGodzinaObecnosciOd = DateTime.MinValue;
            public DateTime PreferowanaGodzinaObecnosciDo = DateTime.MinValue;
            public int DywizjaId = -1;
            public int JednostkaOrganizacyjnaId = -1;
            public int mozliwoscZamykaniaMiesiaca = 0;
            public int Dekretacja = 0;
            public int WysylajPowiadomieniaEmail = 1;
            public int ZmieniajDanePrzelozonychPodczasImportu = 1;
            public int ZmieniajDaneLimitowUrlopowychPodczasImportu = 1;
            public int PrzejdzDoKartyPracyPoSzybkiejRejestracjiZdarzen = 1;
            public int SzybkaRejestracjaZdarzenia = 0;
            public int MozliwoscLogowaniaDoProgramu = 1;
            public int MozliwoscWyswietleniaWiadomosciSystemowych = 0;
            public int Zapomniany = 0;
            public string DaneHaslaTymczasowego = "";
            public DateTime DataOsiagniecia10LatStazu = DateTime.MinValue;
            public TakNie PierwszaPraca = TakNie.Tak;
            public int CzasTrwaniaDnia = 480;
            public DateTime DataUrodzenia = DateTime.MinValue;

        }

        public enum TakNie
        {
            Nie = 0,
            Tak = 1
        }

        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        //    ZapiszRozbicieGodzin(connectionString);
        
           
            Console.ReadKey();
        }

        private static void ZapiszRozbicieGodzin(string connectionString)
        {
            var bulk = new NGVolt.Data.Bulk.BulkCopy<RozbicieGodzin>(connectionString);
            var p = new NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.BulkWriteParams<RozbicieGodzin>("RozbicieGodzin");
            p.ReadIdsInsertedRecords = true;
            bulk.WriteMethod = new NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.BulkWriteWithResult<RozbicieGodzin>(p);

            var rec = new RozbicieGodzin();
            rec.IdKartoteka = 468;
            rec.Pochodzenie_IdKalkulacje = 103668;
            rec.Pochodzenie_IdKalkulacje_TypSkladnika = 1;
            rec.Pochodzenie = 1;
            rec.Data = new DateTime(2020, 01, 20);
            rec.LiczbaMinut = 1;
            rec.Id = 171;
            bulk.RecordsToWrite.Add(rec, NGVolt.Data.Bulk.RowDestinationState.ToUpdate);

            var result = bulk.WriteToSerwer();
        }
    }
}
