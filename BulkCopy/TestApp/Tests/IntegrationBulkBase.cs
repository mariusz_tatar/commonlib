﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkCopy.Tests
{
    public abstract class IntegrationBulkBase
    {
        protected readonly string connectionString;

        public IntegrationBulkBase(string connectionString)
        {
            this.connectionString = connectionString;
        }


        protected void CreateTable(out string tableSyntheticsName, out string tableAnaliticsName)
        {
            tableAnaliticsName = "RecordAnalitic_" + System.Diagnostics.Stopwatch.GetTimestamp();
            tableSyntheticsName = "RecordSyntetic_" + System.Diagnostics.Stopwatch.GetTimestamp();

            string sql = string.Format(@"

                        if  OBJECT_ID('{1}') is  not null
	                        drop table {1}

                        if  OBJECT_ID('{0}') is  not null
	                        drop table {0}


                        create table {0} (
	                        Id 						int identity (100, 1) not null,	
	                        Name					nvarchar(50) not null default '',
	                        CreationDate			DateTime2 not null,
                            TimeStamp               bigint not null,

	                        CONSTRAINT PKCL_{0}_Id PRIMARY KEY CLUSTERED (Id)
                        )

                        create table {1} (
	                        Id 						int identity (100, 1) not null,	
	                        IdSynetic		int not null,
	                        Value			nvarchar(50) not null default '',
	                        CreationDate	DateTime2 not null,
                            TimeStamp               bigint not null,

	                        CONSTRAINT PKCL_{1}_Id PRIMARY KEY CLUSTERED (Id),
	                        CONSTRAINT FK_{1}____{0}_Id FOREIGN KEY (IdSynetic) REFERENCES {0} (Id)
                        )
                    CREATE UNIQUE INDEX UNIQUE_INDEX_{0} ON {0} (TimeStamp);
                    CREATE UNIQUE INDEX UNIQUE_INDEX_{1} ON {1} (TimeStamp);
            ", tableSyntheticsName, tableAnaliticsName);

            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                new NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.SqlQueryManager.SqlServerManager().ExecuteSQL(sql, connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Bład tqorzenia tabel");
            }
            finally
            {
                if (connection != null && connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }



        public class RecordSyntetic : IEquatable<RecordSyntetic>
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime CreationDate { get; set; }
            public long TimeStamp { get; set; }

            public static bool operator ==(RecordSyntetic a, RecordSyntetic b)
            {
                if (a != null)
                    return a.Equals(b);

                return a == null && b == null;
            }

            public static bool operator !=(RecordSyntetic a, RecordSyntetic b)
            {
                return !(a == b);
            }

            public bool Equals(RecordSyntetic other)
            {
                if (other == null)
                    return false;

                if (ReferenceEquals(this, other))
                    return true;

                return Id == other.Id &&
                    Name.Equals(other.Name, StringComparison.Ordinal) &&
                    CreationDate == other.CreationDate &&
                    TimeStamp == other.TimeStamp;
            }
        }

        public class RecordAnalitic
        {
            public int Id { get; set; }
            public int IdSynetic { get; set; }
            public string Value { get; set; }
            public DateTime CreationDate { get; set; }
            public long TimeStamp { get; set; }

            public static bool operator ==(RecordAnalitic a, RecordAnalitic b)
            {
                if (a != null)
                    return a.Equals(b);

                return a == null && b == null;
            }

            public static bool operator !=(RecordAnalitic a, RecordAnalitic b)
            {
                return !(a == b);
            }

            public bool Equals(RecordAnalitic other)
            {
                if (other == null)
                    return false;

                if (ReferenceEquals(this, other))
                    return true;

                return Id == other.Id &&
                    IdSynetic == other.IdSynetic &&
                    Value.Equals(other.Value, StringComparison.Ordinal) &&
                    CreationDate == other.CreationDate &&
                    TimeStamp == other.TimeStamp;
            }
        }
    }
}
