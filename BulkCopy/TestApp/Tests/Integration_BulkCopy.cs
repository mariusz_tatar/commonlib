﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NGVolt.Data.Bulk;
using NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult;

namespace BulkCopy.Tests
{
    [TestClass]
    public class Integration_BulkCopy : IntegrationBulkBase
    {

        public Integration_BulkCopy()
            : base(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)
        {
        }


        [TestMethod]
        public void InsertNoRecordToTable_ExpectSuccess()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);
                var bulk = new BulkCopy<RecordSyntetic>(connectionString);
                var p = new BulkWriteParams<RecordSyntetic>(syntetic);
                p.ReadIdsInsertedRecords = false;
                bulk.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(p);
                
                var ret = bulk.WriteToSerwer();
                ret.Should().NotBeNull();
                ret.Error.Should().BeNull();
                ret.Success.Should().BeTrue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }

        [TestMethod]
        public void Insert1000Records_ExpectSuccessReturnIds()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);
                var bulk = new BulkCopy<RecordSyntetic>(connectionString);
                var parameters = new BulkWriteParams<RecordSyntetic>(syntetic);
                parameters.ReadIdsInsertedRecords = true;
                parameters.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulk.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(parameters);
                bulk.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var ret = bulk.WriteToSerwer();

                ret.Should().NotBeNull();
                ret.Error.Should().BeNull();
                ret.Success.Should().BeTrue();
                var allRecordsFromDB = dbHelper.ReadRecords<RecordSyntetic>(syntetic, null);
                bulk.RecordsToWrite.AllRecords.Count().Should().Be(allRecordsFromDB.Count);

                foreach (var recordFromDB in allRecordsFromDB)
                {
                    var rec = bulk.RecordsToWrite.AllRecords.Single(x => x.Record.TimeStamp == recordFromDB.TimeStamp).Record;
                    recordFromDB.Should().BeEquivalentTo(rec);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }



        [TestMethod]
        public void Insert1000Records_ExpectSuccessWithOutReturnIds()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);
                var bulk = new BulkCopy<RecordSyntetic>(connectionString);
                var p = new BulkWriteParams<RecordSyntetic>(syntetic);
                p.ReadIdsInsertedRecords = false;
                bulk.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(p);
                bulk.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var ret = bulk.WriteToSerwer();

                ret.Should().NotBeNull();
                ret.Error.Should().BeNull();
                ret.Success.Should().BeTrue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }

        [TestMethod]
        public void InsertAndUpdate1000Records_ExpectSuccessWithReturnIds()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);

                //dodanie 1 części rekordó
                var bulk = new BulkCopy<RecordSyntetic>(connectionString);
                var p = new BulkWriteParams<RecordSyntetic>(syntetic);
                p.ReadIdsInsertedRecords = false;
                bulk.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(p);
                bulk.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var ret = bulk.WriteToSerwer();

                //dodanie 2 części rekorów + zmodyfikowane rekordy z 1 części
                var recordsFromDB = dbHelper.ReadRecords<RecordSyntetic>(syntetic, "");
                var bulkInsertUpdate = new BulkCopy<RecordSyntetic>(connectionString);
                var paramsBulkInsertUpdate = new BulkWriteParams<RecordSyntetic>(syntetic);
                paramsBulkInsertUpdate.ReadIdsInsertedRecords = true;
                paramsBulkInsertUpdate.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulkInsertUpdate.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(paramsBulkInsertUpdate);
                recordsFromDB.ForEach(x => x.Name += "_edit");

                var recordsToInsert = Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() });
                var randomizedRecords = recordsFromDB.Concat(recordsToInsert).ToList().Randomize();

                foreach (var rec in randomizedRecords)
                    bulkInsertUpdate.RecordsToWrite.Add(rec, rec.Id > 0 ? RowDestinationState.ToUpdate : RowDestinationState.ToInsert);

                var returnInsertUpdate = bulkInsertUpdate.WriteToSerwer();


                //test
                returnInsertUpdate.Should().NotBeNull();
                returnInsertUpdate.Error.Should().BeNull();
                returnInsertUpdate.Success.Should().BeTrue();
                var allRecordsFromDB = dbHelper.ReadRecords<RecordSyntetic>(syntetic, "");

                foreach (var rec in allRecordsFromDB)
                {
                    var insertedOrUpdatetRecord = bulkInsertUpdate.RecordsToWrite.AllRecords.Single(x => x.Record.Id == rec.Id).Record;
                    rec.Should().BeEquivalentTo(insertedOrUpdatetRecord);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }

        [TestMethod]
        public void Insert1000RecordsSyntehticsAndAnalitics_ExpectSuccessWithReturnIDs()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);

                //dodanie rekordów syntetyki
                var bulkSyntetic = new BulkCopy<RecordSyntetic>(connectionString);
                var paramsSyntetic = new BulkWriteParams<RecordSyntetic>(syntetic);
                paramsSyntetic.ReadIdsInsertedRecords = true;
                paramsSyntetic.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulkSyntetic.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(paramsSyntetic);
                bulkSyntetic.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var returnSyntetic = bulkSyntetic.WriteToSerwer();

                //dodanie rekordów analityki powiazane z syntetyką
                var bulkAnalitics = new BulkCopy<RecordAnalitic>(connectionString);
                var paramsAnalitics = new BulkWriteParams<RecordAnalitic>(analitic);
                paramsAnalitics.ReadIdsInsertedRecords = true;
                paramsAnalitics.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulkAnalitics.WriteMethod = new BulkWriteWithResult<RecordAnalitic>(paramsAnalitics);

                foreach (var rec in bulkSyntetic.RecordsToWrite.AllRecords)
                    bulkAnalitics.RecordsToWrite.AddMany(Enumerable.Range(0, 100).Select(x => new RecordAnalitic() { CreationDate = DateTime.Now, IdSynetic = rec.Record.Id, Value = rec.Record.Id + "_" + x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);

                var returnAnalitics = bulkAnalitics.WriteToSerwer();


                //test
                //test synetetyki
                returnSyntetic.Should().NotBeNull();
                returnSyntetic.Error.Should().BeNull();
                returnSyntetic.Success.Should().BeTrue();
                var allRecordsSynteticFromDB = dbHelper.ReadRecords<RecordSyntetic>(syntetic, null);
                bulkSyntetic.RecordsToWrite.AllRecords.Count().Should().Be(allRecordsSynteticFromDB.Count);

                foreach (var recordFromDB in allRecordsSynteticFromDB)
                {
                    var rec = bulkSyntetic.RecordsToWrite.AllRecords.Single(x => x.Record.TimeStamp == recordFromDB.TimeStamp).Record;
                    recordFromDB.Should().BeEquivalentTo(rec);
                }

                //test analityki
                returnAnalitics.Should().NotBeNull();
                returnAnalitics.Error.Should().BeNull();
                returnAnalitics.Success.Should().BeTrue();
                var allRecordsAnaliticsFromDB = dbHelper.ReadRecords<RecordAnalitic>(analitic, null);
                bulkAnalitics.RecordsToWrite.AllRecords.Count().Should().Be(allRecordsAnaliticsFromDB.Count);
                var insertedRecords = bulkAnalitics.RecordsToWrite.AllRecords.ToDictionary(k => k.Record.TimeStamp, v => v.Record);

                foreach (var recordFromDB in allRecordsAnaliticsFromDB)
                {
                    var rec = insertedRecords[recordFromDB.TimeStamp];
                    recordFromDB.Should().BeEquivalentTo(rec);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }

        [TestMethod]
        public void InsertOneMilionRecordsAnalitics_ExpectSuccessWithReturnIDsPerformanceTest()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);

                //dodanie rekordów syntetyki
                var bulkSyntetic = new BulkCopy<RecordSyntetic>(connectionString);
                var paramsSyntetic = new BulkWriteParams<RecordSyntetic>(syntetic);
                paramsSyntetic.ReadIdsInsertedRecords = true;
                paramsSyntetic.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulkSyntetic.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(paramsSyntetic);
                bulkSyntetic.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var returnSyntetic = bulkSyntetic.WriteToSerwer();

                //dodanie rekordów analityki powiazane z syntetyką
                var bulkAnalitics = new BulkCopy<RecordAnalitic>(connectionString);
                var paramsAnalitics = new BulkWriteParams<RecordAnalitic>(analitic);
                paramsAnalitics.ReadIdsInsertedRecords = true;
                paramsAnalitics.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulkAnalitics.WriteMethod = new BulkWriteWithResult<RecordAnalitic>(paramsAnalitics);

                foreach (var rec in bulkSyntetic.RecordsToWrite.AllRecords)
                    bulkAnalitics.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordAnalitic() { CreationDate = DateTime.Now, IdSynetic = rec.Record.Id, Value = rec.Record.Id + "_" + x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);

                var returnAnalitics = bulkAnalitics.WriteToSerwer();


                //test
                //test synetetyki
                returnSyntetic.Should().NotBeNull();
                returnSyntetic.Error.Should().BeNull();
                returnSyntetic.Success.Should().BeTrue();
                var allRecordsSynteticFromDB = dbHelper.ReadRecords<RecordSyntetic>(syntetic, null);
                bulkSyntetic.RecordsToWrite.AllRecords.Count().Should().Be(allRecordsSynteticFromDB.Count);

                foreach (var recordFromDB in allRecordsSynteticFromDB)
                {
                    var rec = bulkSyntetic.RecordsToWrite.AllRecords.Single(x => x.Record.TimeStamp == recordFromDB.TimeStamp).Record;
                    recordFromDB.Should().BeEquivalentTo(rec);
                }

                //test analityki
                returnAnalitics.Should().NotBeNull();
                returnAnalitics.Error.Should().BeNull();
                returnAnalitics.Success.Should().BeTrue();
                var allRecordsAnaliticsFromDB = dbHelper.ReadRecords<RecordAnalitic>(analitic, null);
                bulkAnalitics.RecordsToWrite.AllRecords.Count().Should().Be(allRecordsAnaliticsFromDB.Count);
                var insertedRecords = bulkAnalitics.RecordsToWrite.AllRecords.ToDictionary(k => k.Record.TimeStamp, v => v.Record);

                foreach (var recordFromDB in allRecordsAnaliticsFromDB)
                {
                    var rec = insertedRecords[recordFromDB.TimeStamp];
                    recordFromDB.Should().BeEquivalentTo(rec);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }
    }
}
