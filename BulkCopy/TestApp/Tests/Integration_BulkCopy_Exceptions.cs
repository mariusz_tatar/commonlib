﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NGVolt.Data.Bulk;
using NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult;
using static BulkCopy.Tests.IntegrationBulkBase;

namespace BulkCopy.Tests
{
    [TestClass]
    public class Integration_BulkCopy_Exceptions : IntegrationBulkBase
    {
        public Integration_BulkCopy_Exceptions()
            : base(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)
        {
        }


        [TestMethod]
        public void Insert1000RecordsWithAllForeignKeysError_ExpectErrorAndNoRecordsInserted()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);
                var bulk = new BulkCopy<RecordAnalitic>(connectionString);
                var parameters = new BulkWriteParams<RecordAnalitic>(analitic);
                parameters.ReadIdsInsertedRecords = true;
                parameters.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulk.WriteMethod = new BulkWriteWithResult<RecordAnalitic>(parameters);
                bulk.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordAnalitic() { IdSynetic = 0, CreationDate = DateTime.Now, Value = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var ret = bulk.WriteToSerwer();

                ret.Should().NotBeNull();
                ret.Error.Should().NotBeNull();
                ret.Success.Should().BeFalse();
                var allRecordsFromDB = dbHelper.ReadRecords<RecordAnalitic>(syntetic, null);
                allRecordsFromDB.Count().Should().Be(0);
                bulk.RecordsToWrite.AllRecords.All(x => x.SaveID == 0).Should().BeTrue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }

        [TestMethod]
        public void InsertRecordsWithManyForeignKeyError_ExpectErrorAndNoRecordsInserted()
        {
            string syntetic = "";
            string analitic = "";
            var dbHelper = new DBHelper(connectionString);

            try
            {
                CreateTable(out syntetic, out analitic);
                var bulkSyntetic = new BulkCopy<RecordSyntetic>(connectionString);
                var parametersSyntetic = new BulkWriteParams<RecordSyntetic>(syntetic);
                parametersSyntetic.ReadIdsInsertedRecords = true;
                parametersSyntetic.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulkSyntetic.WriteMethod = new BulkWriteWithResult<RecordSyntetic>(parametersSyntetic);
                bulkSyntetic.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordSyntetic() { CreationDate = DateTime.Now, Name = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                bulkSyntetic.WriteToSerwer();

                var bulk = new BulkCopy<RecordAnalitic>(connectionString);
                var parameters = new BulkWriteParams<RecordAnalitic>(analitic);
                parameters.ReadIdsInsertedRecords = true;
                parameters.UpdateIdRecordMethod = (rec) => rec.Record.Id = rec.SaveID;
                bulk.WriteMethod = new BulkWriteWithResult<RecordAnalitic>(parameters);
                bulk.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordAnalitic() { IdSynetic = 0, CreationDate = DateTime.Now, Value = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                bulk.RecordsToWrite.AddMany(Enumerable.Range(0, 1000).Select(x => new RecordAnalitic() { IdSynetic = bulkSyntetic.RecordsToWrite.AllRecords[x].Record.Id, CreationDate = DateTime.Now, Value = x.ToString(), TimeStamp = System.Diagnostics.Stopwatch.GetTimestamp() }), RowDestinationState.ToInsert);
                var ret = bulk.WriteToSerwer();

                ret.Should().NotBeNull();
                ret.Error.Should().NotBeNull();
                ret.Success.Should().BeFalse();
                dbHelper.ReadRecords<RecordSyntetic>(syntetic, null).Should().HaveCount(bulkSyntetic.RecordsToWrite.AllRecords.Count);
                dbHelper.ReadRecords<RecordAnalitic>(analitic, null).Should().HaveCount(0);
                bulk.RecordsToWrite.AllRecords.All(x => x.SaveID == 0).Should().BeTrue();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dbHelper.DropTempTable(analitic);
                dbHelper.DropTempTable(syntetic);
            }
        }
    }
}
