﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkCopy.Tests
{
    public class DBHelper
    {
        private readonly string connectionString;

        public DBHelper(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<T> ReadRecords<T>(string tableName, string sqlWhere) where T : new()
        {
            SqlDataReader reader = null;
            var member = FastMember.TypeAccessor.Create(typeof(T));
            var properies = member.GetMembers();
            List<T> returnList = new List<T>();


            try
            {
                GetConnection(sqlConnection =>
                {
                    string sql = "select * from " + tableName;

                    if (string.IsNullOrWhiteSpace(sqlWhere) == false)
                        sql += " WHERE " + sqlWhere;

                    var cmd = new SqlCommand(sql, sqlConnection);
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var rec = new T();

                        foreach (var p in properies.Where(x => x.CanWrite))
                            member[rec, p.Name] = reader[p.Name];

                        returnList.Add(rec);
                    }
                });

                return returnList;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (reader != null && reader.IsClosed == false)
                    reader.Close();
            }

        }

        private void GetConnection(Action<SqlConnection> action)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                action(sqlConnection);
            }
        }


        public void DropTempTable(string tableName)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                return;

            string sql = string.Format(@"  if  OBJECT_ID('{0}') is  not null
                                         drop table {0}", tableName);

            GetConnection(sqlConnection =>
            {
                new SqlCommand(sql, sqlConnection).ExecuteNonQuery();
            });
        }

    }
}
