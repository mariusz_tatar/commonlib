﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGVolt.Data.Bulk.Transaction
{
    public interface ITransaction : IDisposable
    {
        void Complete();
    }
}
