﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGVolt.Data.Bulk.Transaction
{
    public class FakeTransaction : ITransaction, IDisposable
    {
        private bool disposedValue = false;

        public void Complete()
        {
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
                disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }

    }
}
