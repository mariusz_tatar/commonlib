﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace NGVolt.Data.Bulk.Transaction
{
    public class DBTransaction : ITransaction
    {
        private TransactionScope _transaction;
        private bool disposedValue = false;



        public DBTransaction()
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = new TimeSpan(0, 0, 10, 0, 0)
            };

            _transaction = new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }


        public void Complete()
        {
            _transaction.Complete();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _transaction.Dispose();
                }

                disposedValue = true;
            }
        }


        public void Dispose()
        {
            Dispose(true);
        }
    }
}
