﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGVolt.Data.Bulk
{
    public class DebugInformation
    {
        private StringBuilder _sbLog;
        public string DebugLog { get { return _sbLog.ToString(); } }
        public bool LoggingEnabled { get; set; }


        public DebugInformation()
        {
            _sbLog = new System.Text.StringBuilder();
            LoggingEnabled = true;
        }

        internal void Debug(Func<string> action)
        {
            if (LoggingEnabled)
                _sbLog.AppendLine(string.Format("[{0}] - {1}", DateTime.Now, action()));
        }

    }
}
