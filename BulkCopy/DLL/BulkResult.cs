﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk
{
    public class BulkResult
    {
        public System.Diagnostics.Stopwatch ExecutionTimie { get; set; }
        public Exception Error { get; set; }
        public bool Success { get; internal set; }

        public DebugInformation DebugInfo { get; private set; }

        public BulkResult()
        {
            DebugInfo = new DebugInformation();
        }
    }
}
