﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace NGVolt.Data.Bulk
{
    public class BulkCopy<TRecord> : IBulkCopy
    {
        private string _connectionString;
        private SqlConnection _connection;
        private bool _useSelfTransaction;

        public RecordsCollection<TRecord> RecordsToWrite { get; private set; }
        public BulkMethods.IBulkMethod<TRecord> WriteMethod { get; set; }



        public BulkCopy(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _useSelfTransaction = true;
            RecordsToWrite = new RecordsCollection<TRecord>();
        }

        public BulkCopy(SqlConnection connection, bool useSelfTransaction)
        {
            _connection = connection;
            _useSelfTransaction = useSelfTransaction;
            RecordsToWrite = new RecordsCollection<TRecord>();
        }

        public BulkResult WriteToSerwer()
        {
            return WriteToSerwer(null);
        }

        public BulkResult WriteToSerwer(Action<SqlConnection> actionAfterWriteWithCommonConnection)
        {
            var result = new BulkResult();
            result.ExecutionTimie = System.Diagnostics.Stopwatch.StartNew();

            if (RecordsToWrite.IsAnyRecordsToWriteOrUpdate() == false)
            {
                result.Success = true;
                result.ExecutionTimie.Stop();
                return result;
            }

            if (WriteMethod == null)
                throw new Exception("Brak metody do wpisania danych");

            if (WriteMethod.IsCanUse(RecordsToWrite) == false)
                throw new Exception("Wybrana metoda wpisania danych nie jest obsługiwana dla rekordów");

            WriteAllDataToSerwer(result, actionAfterWriteWithCommonConnection);
            return result;
        }

        private void WriteAllDataToSerwer(BulkResult result, Action<SqlConnection> actionAfterWriteWithCommonConnection)
        {
            using (var scope = GetTransaction())
            {
                try
                {
                    if (_connection.State != ConnectionState.Open)
                        _connection.Open();

                    WriteMethod.WriteToSerwer(RecordsToWrite, result, _connection);

                    if (actionAfterWriteWithCommonConnection != null)
                        actionAfterWriteWithCommonConnection(_connection);

                    scope.Complete();
                    result.ExecutionTimie.Stop();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.ExecutionTimie.Stop();
                    result.Error = ex;
                    result.Success = false;
                }
                finally
                {
                    if (_connection != null && _connection.State == ConnectionState.Open && _useSelfTransaction)
                        _connection.Close();

                    scope.Dispose();
                }
            }
        }

        private Transaction.ITransaction GetTransaction()
        {
            if (_useSelfTransaction == false)
                return new Transaction.FakeTransaction();

            return new Transaction.DBTransaction();
        }

    }
}
