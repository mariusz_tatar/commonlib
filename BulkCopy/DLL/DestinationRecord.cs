﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk
{
    public enum RowDestinationState
    {
        ToInsert,
        ToUpdate
    }

    public class DestinationRecord<TRecord>
    {
        public int SaveID { get; set; }
        public long TimeStamp { get; set; }
        public TRecord Record { get; private set; }
        public RowDestinationState RowState { get; private set; }

        public DestinationRecord(TRecord record, RowDestinationState rowState, RecordsCollection<TRecord> recordsCollection)
        {
            Record = record;
            RowState = rowState;
            recordsCollection.AssignTimestamp(this);
        }
    }
}
