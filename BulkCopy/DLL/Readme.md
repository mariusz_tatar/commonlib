﻿# BulkCopy

Biblioteka umożliwiająca masowe dodawanie i update rekordó w bazie danych.

## Instalacja

Aby zainstalować bibliotekę należy pobrać skompilowaną DLL lub pobrać ją z firmowgo NuGet-a spod adresu [NugetServer96Volt](http://nugetserver.96volt.com/nuget) pod nazwą 'BulkCopy'

## Użycie

```csharp
public static class Zapis
{
     public class Rekord
        {
            public int Id;
            public string Nazwa;
            public DateTime DataDodania;
        }

        public void Zapisz()
        {
            var bulkCopy = new BulkCopy.BulkCopy<Rekord>(connectionstring);
            var parametry = new BulkCopy.BulkMethods.BulkWriteWithResult.BulkWriteParams<Rekord>("NazwaTabeliRekordów");
            parametry.ReadIdsInsertedRecords = true;
            bulkCopy.WriteMethod = new BulkCopy.BulkMethods.BulkWriteWithResult.BulkWriteWithResult<Rekord>(parametry);
            bulkCopy.RecordsToWrite.Add(new Rekord(), RowDestinationState.ToInsert);
            bulkCopy.RecordsToWrite.Add(new Rekord(), RowDestinationState.ToInsert);
            var wynikZapisu = bulkCopy.WriteToSerwer();

            if (wynikZapisu.Success == false)
            {
                Console.WriteLine("Poprawnie dodano");
            }
            else
            {
                Console.WriteLine("Błąd dodania" + wynikZapisu.Error.Message);
            }
        }

        public static void Main()
        {
            Zapisz();  
		}
}
```

## Modyfikacja

Po modyfikacji biblioteki należy zmienić numer wersji w pliku ''BulkCopy.nuspec'' oraz wykonać skrypt ''Publikowanie pakietów nuget.ps1''


