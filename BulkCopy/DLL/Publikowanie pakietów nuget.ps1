$key="96volt"
$filename = "C:\Source\CommonLib\BulkCopy\DLL\BulkCopy.nuspec"
[xml]$xml = Get-Content $filename
$version=write $xml.SelectNodes('//package/metadata/version').InnerText
$nugetURL="http://nugetserver.96volt.com/nuget"
$fileName=(-join("BulkCopy.",$version,".nupkg"))
$params=(-join("push ",$fileName," ",$key," -Source ",$nugetURL))

write "Pakowanie..."
& .\nuget.exe @("pack","BulkCopy.csproj","-Properties", "Configuration=Release")

write  "Wysy�anie..."
&  .\nuget.exe @("push", $fileName, $key, "-Source", $nugetURL,"-SkipDuplicate")
