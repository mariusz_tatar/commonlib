﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk
{
    public class RecordsCollection<TRecord>
    {
        private long _localTimestamp;
        private readonly ReaderWriterLock rwLock;
        private List<DestinationRecord<TRecord>> _allRecords { get; set; }
        public ReadOnlyCollection<DestinationRecord<TRecord>> AllRecords { get { return _allRecords.AsReadOnly(); } }


        public RecordsCollection()
        {
            _localTimestamp = 0;
            rwLock = new ReaderWriterLock();
            _allRecords = new List<DestinationRecord<TRecord>>();
        }


        public void AddMany(IEnumerable<TRecord> records, RowDestinationState destState)
        {
            _allRecords.AddRange(records.Select(x => new DestinationRecord<TRecord>(x, destState, this)));
        }

        internal void AssignTimestamp(DestinationRecord<TRecord> destinationRecord)
        {
            rwLock.AcquireReaderLock(Timeout.Infinite);

            try
            {
                Interlocked.Increment(ref _localTimestamp);
                destinationRecord.TimeStamp = _localTimestamp;
            }
            finally
            {
                rwLock.ReleaseReaderLock();
            }

        }

        internal long GetLocalTimeStamp()
        {
            Interlocked.Increment(ref _localTimestamp);
            return _localTimestamp;
        }

        public void Add(TRecord record, RowDestinationState destState)
        {
            _allRecords.Add(new DestinationRecord<TRecord>(record, destState, this));
        }

        public IEnumerable<DestinationRecord<TRecord>> GetSpecifedRecords(RowDestinationState state)
        {
            return _allRecords.Where(x => x.RowState == state);
        }

        internal bool IsAnyRecordsToWriteOrUpdate()
        {
            return _allRecords != null && _allRecords.Any();
        }
    }
}
