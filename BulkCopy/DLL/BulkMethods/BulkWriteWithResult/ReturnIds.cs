﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulkCopy.BulkMethods.BulkWriteWithResult
{
   public class ReturnIds<T>
    {
        public T ReturnID { get; set; }
        public long RecordTimeStamp { get; set; }

        public ReturnIds(T returnID, long timestamp)
        {
            ReturnID = returnID;
            RecordTimeStamp = timestamp;
        }
    }
}
