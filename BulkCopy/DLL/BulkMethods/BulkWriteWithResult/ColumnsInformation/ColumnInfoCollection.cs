﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.ColumnInformation
{
    public class ColumnInfoCollection
    {
        public string EntityName { get; private set; }
        private Dictionary<string, ColumnInfo> _allColumns;
        public DataTable TableWithStructure { get; set; }
        public System.Collections.ObjectModel.ReadOnlyCollection<ColumnInfo> AllColumns { get { return _allColumns.Values.ToList().AsReadOnly(); } }
        public ColumnInfo PrimaryKey { get { return _allColumns.Values.FirstOrDefault(x => x.IsPrimaryKey); } }


        public ColumnInfoCollection(string entityName)
        {
            EntityName = entityName;
            _allColumns = new Dictionary<string, ColumnInfo>();
        }

        public void AddColumn(ColumnInfo columnInfo)
        {
            GetColumn(columnInfo.Name, columnInfo);
        }

        public ColumnInfo GetColumn(string name, ColumnInfo addIfNotExists = null)
        {
            ColumnInfo columnInfo = null;

            if (_allColumns.TryGetValue(name.ToLower(), out columnInfo) == false)
            {
                if (addIfNotExists != null)
                {
                    _allColumns.Add(addIfNotExists.Name.ToLower(), addIfNotExists);
                    return addIfNotExists;
                }
            }
            return columnInfo;
        }

        internal void AddTimeStampColumn(string timeStampColumnName)
        {
            AddColumn(ColumnInfo.GetTimestampColumn(timeStampColumnName));
            TableWithStructure.Columns.Add(timeStampColumnName, typeof(long));
        }
    }
}
