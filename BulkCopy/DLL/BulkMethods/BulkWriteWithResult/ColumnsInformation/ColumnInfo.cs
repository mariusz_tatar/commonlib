﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.ColumnInformation
{
    public class ColumnInfo
    {
        public string Name { get; set; }
        public bool AllowDBNull { get; set; }
        public bool IsEnum { get; internal set; }
        public bool IsPrimaryKey { get; set; }
        public bool IsVirtualTimeStampColumn { get; set; }

        public ColumnInfo(string columnName)
        {
            Name = columnName;
        }

        public static ColumnInfo GetTimestampColumn(string name)
        {
            return new ColumnInfo(name)
            {
                AllowDBNull=false,
                IsEnum=false,
                IsPrimaryKey=false,
                IsVirtualTimeStampColumn=true

            };
        }
        public override string ToString()
        {
            return string.Format("Name: {0}, AllowDBNull: {1}, IsEnum: {2}, IsPrimaryKey: {3}, IsVirtualTimeStampColumn: {4}", Name, AllowDBNull, IsEnum, IsPrimaryKey, IsVirtualTimeStampColumn);
        }
    }
}
