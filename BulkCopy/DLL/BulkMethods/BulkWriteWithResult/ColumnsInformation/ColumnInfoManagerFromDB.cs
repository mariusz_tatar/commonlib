﻿using NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.SqlQueryManager;
using FastMember;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.ColumnInformation
{
    /// <summary>
    /// Klasa pobiera z bazy danych na podstawie nazwy tabeli wszystkie informacje dotyczące kolumn w tabeli.
    /// </summary>
    public class ColumnInfoManagerFromDB : IColumnInformationManager
    {
        private SqlQueryManager.ISqlQueryExecutor _queryManager;




        public ColumnInfoManagerFromDB(ISqlQueryExecutor queryManager)
        {
            _queryManager = queryManager;
        }

        /// <summary>
        /// Zwraca wszystkie kolumny w tabeli wraz z informacją o nich, czy dbnull czy enum itp.
        /// Wszystkie oepracje wykonuje na podstawie tabeli w bazie danych.
        /// </summary>
        public ColumnInfoCollection GetColumnInfo<TRecord>(string tableNameWithSchema, string schema, string tableName, SqlConnection sqlConnection)
        {
            ColumnInfoCollection result = new ColumnInfoCollection(tableNameWithSchema);
            GetStrucureOfTable(result, tableNameWithSchema, sqlConnection);
            GetAllInformationForColumn<TRecord>(result, sqlConnection, schema, tableName);
            return result;
        }

        private void GetStrucureOfTable(ColumnInfoCollection result, string tableNameWithSchema, SqlConnection sqlConnection)
        {
            result.TableWithStructure = GetDataTableWithStructure(sqlConnection, tableNameWithSchema);

            foreach (var column in result.TableWithStructure.Columns.Cast<DataColumn>())
                result.AddColumn(new ColumnInfo(column.ColumnName));
        }

        private DataTable GetDataTableWithStructure(SqlConnection connection, string tableNameWithSchema)
        {
            var table = new DataTable();
            SqlCommand cmd = new SqlCommand(string.Format("select * from {0} where 1=0", tableNameWithSchema), connection);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(table);
            return table;
        }

        private void GetAllInformationForColumn<TRecord>(ColumnInfoCollection result, SqlConnection sqlConnection, string schema, string tableName)
        {
            UpdateColumnsInfoFromDataBase(sqlConnection, schema, tableName, result);
            SelectEnumColumns<TRecord>(result);
        }

        private void UpdateColumnsInfoFromDataBase(SqlConnection sqlConnection, string schema, string tableName, ColumnInfoCollection columnInfo)
        {
            string sql = @"select	
                                syscolumns.name,
		                        cast( syscolumns.isnullable as bit) as isnullable,
		                        cast((case  when pk.COLUMN_NAME is null then 0 else 1 end) as bit) as isPrimaryKey
                            from   sysobjects 
	                            join syscolumns on  sysobjects.id = syscolumns.id  and  sysobjects.xtype = 'U'  and  sysobjects.name  = '{0}'
	                            left join (select tc.CONSTRAINT_TYPE,ku.COLUMN_NAME
				                            FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
					                            INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
						                            ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND KU.table_name='{0}' and ku.TABLE_SCHEMA='{1}'
			                              ) pk on pk.COLUMN_NAME=syscolumns.name";

            sql = string.Format(sql, tableName, schema);

            _queryManager.ExexuteSQLWithReader(sql, sqlConnection, (reader) =>
            {
                string columnName = (string)reader["name"];
                columnInfo.GetColumn(columnName).AllowDBNull = (bool)reader["isnullable"];
                columnInfo.GetColumn(columnName).IsPrimaryKey = (bool)reader["isPrimaryKey"];
            });
        }

        private void SelectEnumColumns<TRecord>(ColumnInfoCollection result)
        {
            var recordType = typeof(TRecord);
            var accesor = TypeAccessor.Create(recordType);
            var members = accesor.GetMembers().ToDictionary(k => k.Name.ToLower(), v => v);
            //var allFields = recordType.GetFields().Where(x => x.IsPublic).ToDictionary(k => k.Name.ToLower(), v => v);
            // var allProperies = recordType.GetProperties().ToDictionary(k => k.Name.ToLower(), v => v);

            foreach (var column in result.AllColumns)
            {
                string columnNameKey = column.Name.ToLower();
                var dataTableColumn = result.TableWithStructure.Columns[column.Name];

                // FieldInfo fieldInfo = null;
                // PropertyInfo propertyInfo = null;
                Member member = null;

                if (members.TryGetValue(columnNameKey, out member) && member.Type.IsEnum)
                    column.IsEnum = true;
            }
        }
    }
}
