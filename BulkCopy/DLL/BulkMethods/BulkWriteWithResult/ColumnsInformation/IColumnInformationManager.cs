﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.ColumnInformation
{
    /// <summary>
    /// Interfejs do pobierania inforamcji o tabeli i jej kolumnach
    /// </summary>
    public interface IColumnInformationManager
    {
        /// <summary>
        /// Zwraca wszystkie kolumny w tabeli wraz z informacją o nich, czy dbnull czy enum itp.
        /// </summary>
        ColumnInfoCollection GetColumnInfo<TRecord>(string tableNameWithShema, string schema, string tableName, SqlConnection sqlConnection);
    }
}
