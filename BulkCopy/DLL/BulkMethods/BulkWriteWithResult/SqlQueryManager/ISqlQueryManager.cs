﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.SqlQueryManager
{
    /// <summary>
    /// Interfejs wykonywania zapytań SQL
    /// </summary>
    public interface ISqlQueryExecutor
    {
        void ExecuteSQL(string sql, SqlConnection sqlConnection);
        void ExexuteSQLWithReader(string sql, SqlConnection sqlConnection, Action<SqlDataReader> readerAction);
    }
}
