﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.SqlQueryManager
{
    public class SqlServerManager : ISqlQueryExecutor
    {
        public void ExecuteSQL(string sql, SqlConnection sqlConnection)
        {
            new SqlCommand(sql, sqlConnection).ExecuteNonQuery();
        }

        public void ExexuteSQLWithReader(string sql, SqlConnection sqlConnection, Action<SqlDataReader> readerAction)
        {
            SqlDataReader reader = null;

            try
            {
                var cmd = new SqlCommand(sql, sqlConnection);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                    readerAction(reader);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (reader != null && reader.IsClosed == false)
                    reader.Close();
            }

        }
    }
}
