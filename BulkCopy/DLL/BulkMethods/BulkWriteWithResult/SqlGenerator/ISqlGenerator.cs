﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.SqlGenerator
{
    /// <summary>
    /// Interfejs do generowania SQL
    /// </summary>
    public  interface ISqlGenerator
    {
        string DropTempTable(string tempTableName);
        string CreateTempTableWithTimeStampColumn(string tempTable, string sourceTable,string timeStampColumnName);
        string CreateMergeFromTempTableToDestinationTable(string tempTableName, string destinationTableName, string timestampColumnName, IEnumerable<string> columnNames, IEnumerable<string> primaryKeysColumnNames, bool withOutputIds);
    }
}
