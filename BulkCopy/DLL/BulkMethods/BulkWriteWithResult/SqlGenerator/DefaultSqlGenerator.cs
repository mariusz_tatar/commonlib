﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.SqlGenerator
{
    public class DefaultSqlGenerator : ISqlGenerator
    {
        public string CreateTempTableWithTimeStampColumn(string tempTable, string sourceTable, string timeStampColumnName)
        {
            string baseSQL = string.Format("SELECT tablee.* INTO #{0} FROM {1} tablee WHERE 1=0; ", tempTable, sourceTable);

            if (string.IsNullOrWhiteSpace(timeStampColumnName)==false)
                return baseSQL + string.Format(" alter table #{0} add {1} bigint not null", tempTable, timeStampColumnName);

            return baseSQL;
        }

        public string CreateMergeFromTempTableToDestinationTable(string tempTableName, string destinationTableName, string timestampColumnName, IEnumerable<string> columnNames, IEnumerable<string> primaryKeysColumnNames, bool withOutputIds)
        {
            var columnsWithoutPrimaryKeysAndVirtual = columnNames.Where(x => primaryKeysColumnNames.Any(y => y.Equals(x, StringComparison.OrdinalIgnoreCase)) == false);

            if (withOutputIds)
                columnsWithoutPrimaryKeysAndVirtual = columnsWithoutPrimaryKeysAndVirtual.Where(x => x.Equals(timestampColumnName, StringComparison.OrdinalIgnoreCase) == false);

            string insertSql = string.Format("INSERT ({0}) VALUES({1})", string.Join(",", columnsWithoutPrimaryKeysAndVirtual), string.Join(",", columnsWithoutPrimaryKeysAndVirtual.Select(x => "tempTable." + x)));
            string updateSql = string.Format("UPDATE SET {0}", string.Join(",", columnsWithoutPrimaryKeysAndVirtual.Select(x => "destinationTable." + x + "=tempTable." + x)));

            if (withOutputIds)
            {
                string outputSQL = string.Format(" OUTPUT INSERTED.Id,tempTable.{0}  INTO @insertedTable; ", timestampColumnName);

                string sql = string.Format(@"                            
                            DECLARE @insertedTable TABLE(id int,{5} bigint)

                            MERGE INTO {0} destinationTable
	                        USING #{1} tempTable ON destinationTable.id=tempTable.Id
	                        WHEN MATCHED THEN
                                {2}
	                        WHEN NOT MATCHED THEN	 
                                {3} {4};

                            select * from @insertedTable;", destinationTableName, tempTableName, updateSql, insertSql, outputSQL, timestampColumnName);
                return sql;
            }
            else
            {
                string sql = string.Format(@"                            
                            MERGE INTO {0} destinationTable
	                        USING #{1} tempTable ON destinationTable.id=tempTable.Id
	                        WHEN MATCHED THEN
                                {2}
	                        WHEN NOT MATCHED THEN	 
                                {3};", destinationTableName, tempTableName, updateSql, insertSql);
                return sql;
            }
        }

        public string DropTempTable(string tempTableName)
        {
            string sql = @"IF OBJECT_ID('tempdb..#{0}') IS NOT NULL
                            Truncate TABLE #{0}";
            return string.Format(sql, tempTableName);
        }
    }
}
