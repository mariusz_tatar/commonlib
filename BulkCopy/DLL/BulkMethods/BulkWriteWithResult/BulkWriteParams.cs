﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult
{
    public class BulkWriteParams<TRecord>
    {
        public string Schema
        {
            get
            {
                if (string.IsNullOrEmpty(TableNameWithSchema))
                    return "";

                var split = TableNameWithSchema.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                if (split == null || split.Length <= 1)
                    return "dbo";

                return split[0];
            }
        }
        public string TableNameOnly
        {
            get
            {
                if (string.IsNullOrEmpty(TableNameWithSchema))
                    return "";

                var split = TableNameWithSchema.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                if (split == null || split.Length <= 1)
                    return TableNameWithSchema;

                return split[split.Length - 1];
            }
        }

        /// <summary>
        /// Nazwa tabeli wraz ze schematem
        /// </summary>
        public string TableNameWithSchema { get; set; }

        /// <summary>
        /// Jeśli chcemy aby podczas zapisu nowych rekordów pobrac wynikowe rekordy oraz powiązać je z rekordami należy ustawić tą zmienną na true
        /// Zaznaczenie zmiennej spowoduje, że zapis będzie troszkę wolniejszy ponieważ wykorzystywana jest funkcja sql 'Merge'
        /// </summary>
        public bool ReadIdsInsertedRecords { get; set; }

        /// <summary>
        /// Domyślny SQL generator
        /// </summary>
        public SqlGenerator.ISqlGenerator SqlGenerator { get; set; }

        /// <summary>
        /// Domyślny mechanizm wykonywania zapytań SQL
        /// </summary>
        public SqlQueryManager.ISqlQueryExecutor SqlQueryExecutor { get; set; }

        /// <summary>
        /// Domyślny mechanizm wypełniania tabeli DataTable
        /// </summary>
        public DataTableFillMethod.IDataTableFillMethod DataTableFill { get; set; }

        /// <summary>
        /// Domyślny mechanizm pobierania inforamcji na temat kolumn 
        /// </summary>
        public ColumnInformation.IColumnInformationManager ColumnInfoManager { get; set; }

        public readonly SqlBulkCopyOptions BulkConfigurationFlagsFoInsert = SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction;
        public readonly SqlBulkCopyOptions BulkConfigurationFlagsFoMerge = SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.KeepIdentity | SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction;
        
        /// <summary>
        /// Funkcja która wywoływana jest po zapisie danych do bazy dla wszystkich rekordów Insert. 
        /// Funkcja powinna przepisywać id wynikowy (SaveId) do id poszczególnego rekordu 
        /// </summary>
        public Action<DestinationRecord<TRecord>> UpdateIdRecordMethod;




        public BulkWriteParams(string tableNameWithSchema)
        {
            TableNameWithSchema = tableNameWithSchema;
            SetDefaultProperties();
        }

        private void SetDefaultProperties()
        {
            SqlGenerator = new SqlGenerator.DefaultSqlGenerator();
            SqlQueryExecutor = new SqlQueryManager.SqlServerManager();
            ColumnInfoManager = new ColumnInformation.ColumnInfoManagerFromDB(SqlQueryExecutor);
            DataTableFill = new DataTableFillMethod.FastMemberFiller(-1, DateTime.MinValue);
        }

        public bool IsValid()
        {
            return string.IsNullOrWhiteSpace(TableNameWithSchema) == false && DataTableFill != null && SqlQueryExecutor != null && SqlGenerator != null;
        }

        public override string ToString()
        {
            return string.Format("Schema: {0}, TableNameOnly: {1}, TableNameWithSchema: {2}, ReadIdsInsertedRecords: {3}", Schema, TableNameOnly, TableNameWithSchema, ReadIdsInsertedRecords);
        }
    }
}
