﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.DataTableFillMethod
{
    /// <summary>
    /// Interfejs dodawania danych do tabeli 'DataTable'
    /// </summary>
    public interface IDataTableFillMethod
    {
        /// <summary>
        /// Uzupełnia danymi tabelę wg źródłowych rekordów oraz informacji o kolumnach.
        /// </summary>
        /// <typeparam name="TRecord"></typeparam>
        /// <param name="sourceRecords">Rekordy żródłowe</param>
        /// <param name="destinantionTable">Docelowa tabela do wypełnienia</param>
        /// <param name="columnInfo">Informacje na temat kolumn</param>
        /// <param name="setPrimaryKeyToNull">Faga czy ustawiać na null kolumnę primary key</param>
        void FillRecordsToDataTable<TRecord>(IEnumerable<DestinationRecord<TRecord>> sourceRecords, DataTable destinantionTable, ColumnInformation.ColumnInfoCollection columnInfo, bool setPrimaryKeyToNull);
    }
}
