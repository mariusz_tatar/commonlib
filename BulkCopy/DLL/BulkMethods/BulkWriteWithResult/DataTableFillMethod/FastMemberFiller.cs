﻿
using NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.ColumnInformation;
using FastMember;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.DataTableFillMethod
{
    /// <summary>
    /// Klasa wypełnienia tabeli docelowej wykorzystująca bibliotekę 'FastMember' do szybkiego dostępu do pól klasy (rekordów źródłowych)
    /// </summary>
    public class FastMemberFiller : IDataTableFillMethod
    {
        private int _intConditionValueForDbNull;
        private DateTime _dateConditionValueForDbNull;

        public FastMemberFiller(int intConditionValueForDbNull, DateTime dateConditionValueForDbNull)
        {
            _intConditionValueForDbNull = intConditionValueForDbNull;
            _dateConditionValueForDbNull = dateConditionValueForDbNull;
        }

        public void FillRecordsToDataTable<TRecord>(IEnumerable<DestinationRecord<TRecord>> sourceRecords, DataTable destinantionTable, ColumnInfoCollection columnInfoCollection, bool setPrimaryKeyToNull)
        {
            var accesor = TypeAccessor.Create(typeof(TRecord));
            var allColumns = columnInfoCollection.AllColumns.ToArray();

            foreach (var rec in sourceRecords)
            {
                var rowData = destinantionTable.NewRow();
                int index = 0;

                foreach (var columnInfo in allColumns)
                {
                    object outValue = null;

                    if (columnInfo.IsVirtualTimeStampColumn)
                    {
                        outValue = rec.TimeStamp;
                    }
                    else
                    {
                        outValue = accesor[rec.Record, columnInfo.Name];

                        if (columnInfo.IsPrimaryKey)
                        {
                            if (setPrimaryKeyToNull)
                                outValue = DBNull.Value;
                        }
                        else
                        {
                            if (columnInfo.AllowDBNull && (outValue == null || outValue.Equals(_intConditionValueForDbNull) || outValue.Equals(_dateConditionValueForDbNull)))
                            {
                                outValue = DBNull.Value;
                            }
                            else
                            {
                                if (columnInfo.IsEnum)
                                    outValue = (int)outValue;
                            }
                        }
                    }

                    rowData[index++] = outValue;
                }

                destinantionTable.Rows.Add(rowData);
            }
        }
    }
}
