﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Linq.Dynamic;
using NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult.ColumnInformation;

namespace NGVolt.Data.Bulk.BulkMethods.BulkWriteWithResult
{
    public class BulkWriteWithResult<TRecord> : IBulkMethod<TRecord>
    {
        private readonly string _timeStampColumnName = "_timeStamp_";
        private string _tempTableName = "tempTable_" + DateTime.Now.Ticks;
        public BulkWriteParams<TRecord> Params { get; private set; }
        private int timeoutSeconds;





        public BulkWriteWithResult(BulkWriteParams<TRecord> parameters)
        {
            Params = parameters;
            timeoutSeconds = 60;
        }

        public bool IsCanUse(RecordsCollection<TRecord> recordsCollection)
        {
            return true;
        }

        public void WriteToSerwer(RecordsCollection<TRecord> recordsCollection, BulkResult result, SqlConnection connection)
        {
            if (Params.IsValid() == false)
            {
                result.DebugInfo.Debug(() => "Błędna walidacja parametrów wejściowych: " + Params.ToString());
                result.Success = false;
                result.Error = new Exception("Błędne parametry wejściowe",new Exception(Params.ToString()));
                return;
            }

            if (recordsCollection.IsAnyRecordsToWriteOrUpdate() == false)
            {
                result.DebugInfo.Debug(() => "Brak rekordów do dodania lub aktualizacji");
                result.Success = true;
                return;
            }

            WriteAllRecords(connection, recordsCollection, result);
        }


        private void WriteAllRecords(SqlConnection conn, RecordsCollection<TRecord> recordsCollection, BulkResult result)
        {
            if (Params.ReadIdsInsertedRecords)
                InsertAndUpdateAllRecords(conn, recordsCollection, result);
            else
            {
                InsertOnlyRecords(conn, recordsCollection, result);
                UpdateOnlyRecords(conn, recordsCollection, result);
            }
        }

        private void InsertOnlyRecords(SqlConnection conn, RecordsCollection<TRecord> recordsCollection, BulkResult result)
        {
            IEnumerable<DestinationRecord<TRecord>> recordsToInsert = null;

            if ((recordsToInsert = recordsCollection.GetSpecifedRecords(RowDestinationState.ToInsert)).Any() == false)
                return;

            //odczytanie z bazy całej struktury tabeli + dodanie specjalnej kolumnt timestamp wymaganego przy zwracaniu id dodanych rekordów
            var columnInfo = Params.ColumnInfoManager.GetColumnInfo<TRecord>(Params.TableNameWithSchema, Params.Schema, Params.TableNameOnly, conn);
            result.DebugInfo.Debug(() => "InsertOnlyRecords: Wynikowe informacje o kolumnach źródłowych: " + Environment.NewLine + string.Join(Environment.NewLine, columnInfo.AllColumns.Select(x => x.ToString())));

            if (Params.ReadIdsInsertedRecords)
            {
                columnInfo.AddTimeStampColumn(_timeStampColumnName);
                result.DebugInfo.Debug(() => "InsertOnlyRecords: Dodanie kolumny timestamp do tabeli źródłowej: " + columnInfo.AllColumns.Last().ToString());
            }

            //wypełnienie tabeli DataTable z którego dane będą później wstawiane (razem z timestamp)
            result.DebugInfo.Debug(() => "InsertOnlyRecords: Wypełnienie DataTable...");
            Params.DataTableFill.FillRecordsToDataTable<TRecord>(recordsToInsert, columnInfo.TableWithStructure, columnInfo, true);
            result.DebugInfo.Debug(() => "InsertOnlyRecords: Zakończono");

            //wykonanie
            result.DebugInfo.Debug(() => "InsertOnlyRecords: Bulk do tabeli tymczasowej...");
            ExecuteBulk(conn, columnInfo.TableWithStructure, Params.TableNameWithSchema, Params.BulkConfigurationFlagsFoInsert);
            result.DebugInfo.Debug(() => "InsertOnlyRecords: Zakończono");
        }

        private void UpdateOnlyRecords(SqlConnection conn, RecordsCollection<TRecord> recordsCollection, BulkResult result)
        {
            UpdateRecords(conn, recordsCollection.GetSpecifedRecords(RowDestinationState.ToUpdate), result);
        }

        private void InsertAndUpdateAllRecords(SqlConnection conn, RecordsCollection<TRecord> recordsCollection, BulkResult result)
        {
            var insertRecords = recordsCollection.AllRecords.Where(x => x.RowState == RowDestinationState.ToInsert).ToList();
            var updateRecords = recordsCollection.AllRecords.Where(x => x.RowState == RowDestinationState.ToUpdate).ToList();
            UpdateRecords(conn, updateRecords.Concat(insertRecords), result);
        }

        private void UpdateRecords(SqlConnection conn, IEnumerable<DestinationRecord<TRecord>> recordsToUpdateOrInsert, BulkResult result)
        {
            if (recordsToUpdateOrInsert == null || recordsToUpdateOrInsert.Any() == false)
                return;

            //odczytanie z bazy całej struktury tabeli + dodanie specjalnej kolumnt timestamp wymaganego przy zwracaniu id dodanych rekordów
            var columnInfo = Params.ColumnInfoManager.GetColumnInfo<TRecord>(Params.TableNameWithSchema, Params.Schema, Params.TableNameOnly, conn);
            result.DebugInfo.Debug(() => "UpdateRecords: Wynikowe informacje o kolumnach źródłowych: " + Environment.NewLine + string.Join(Environment.NewLine, columnInfo.AllColumns.Select(x => x.ToString())));

            if (Params.ReadIdsInsertedRecords)
            {
                columnInfo.AddTimeStampColumn(_timeStampColumnName);
                result.DebugInfo.Debug(() => "UpdateRecords: Dodanie kolumny timestamp do tabeli źródłowej: " + columnInfo.AllColumns.Last().ToString());
            }

            //wypełnienie tabeli DataTable z którego dane będą później wstawiane (razem z timestamp)
            result.DebugInfo.Debug(() => "UpdateRecords: Wypełnienie DataTable...");
            Params.DataTableFill.FillRecordsToDataTable<TRecord>(recordsToUpdateOrInsert, columnInfo.TableWithStructure, columnInfo, false);
            result.DebugInfo.Debug(() => "UpdateRecords: Zakończono");

            var sqlTempTable = Params.SqlGenerator.CreateTempTableWithTimeStampColumn(_tempTableName, Params.TableNameWithSchema, Params.ReadIdsInsertedRecords ? _timeStampColumnName : null);
            result.DebugInfo.Debug(() => "UpdateRecords: SQL temp table: " + sqlTempTable);
            Params.SqlQueryExecutor.ExecuteSQL(sqlTempTable, conn);

            result.DebugInfo.Debug(() => "UpdateRecords: Bulk insert do temp table: " + sqlTempTable);
            ExecuteBulk(conn, columnInfo.TableWithStructure, "#" + _tempTableName, Params.BulkConfigurationFlagsFoMerge);
            result.DebugInfo.Debug(() => "UpdateRecords: Bulk insert do temp table: " + sqlTempTable+". Zakończono");

            var returnsId = MergeTempTableToDestination(conn, columnInfo, result);
            Params.SqlQueryExecutor.ExecuteSQL(Params.SqlGenerator.DropTempTable(_tempTableName), conn);
            WriteReturnIDsToSourceRecords(returnsId, recordsToUpdateOrInsert, columnInfo, result);
        }

        private void AddAndFillTimestampColumn(ColumnInfoCollection columnInfo)
        {
            int count = columnInfo.TableWithStructure.Rows.Count;
            columnInfo.TableWithStructure.Columns.Add(_timeStampColumnName, typeof(long));
            columnInfo.AddColumn(ColumnInformation.ColumnInfo.GetTimestampColumn(_timeStampColumnName));

            for (int i = 0; i < count; i++)
                columnInfo.TableWithStructure.Rows[i][_timeStampColumnName] = System.Diagnostics.Stopwatch.GetTimestamp();
        }



        private void WriteReturnIDsToSourceRecords(List<BulkCopy.BulkMethods.BulkWriteWithResult.ReturnIds<int>> returnsId, IEnumerable<DestinationRecord<TRecord>> recordsToUpdateOrInsert, ColumnInformation.ColumnInfoCollection columnInfo, BulkResult result)
        {
            if (recordsToUpdateOrInsert == null || recordsToUpdateOrInsert.Any() == false || Params.ReadIdsInsertedRecords == false)
                return;

            result.DebugInfo.Debug(() => "WriteReturnIDsToSourceRecords: Rozpoczecie...");

            if (columnInfo.PrimaryKey == null)
            {
                result.DebugInfo.Debug(() => "WriteReturnIDsToSourceRecords: Wyjątek!. Brak klucza głównego.Nie można pobrać wynikowych ID");
                throw new Exception("Brak klucza głównego. Nie można pobrać wynikowych ID");
            }

            var returnsIDDictionary = returnsId.ToDictionary(k => k.RecordTimeStamp, v => v);

            foreach (var record in recordsToUpdateOrInsert.Where(x => x.RowState == RowDestinationState.ToInsert))
            {
                record.SaveID = returnsIDDictionary[record.TimeStamp].ReturnID;

                //aktualizacja ID rekordu
                if (Params.UpdateIdRecordMethod != null)
                    Params.UpdateIdRecordMethod(record);
            }

            result.DebugInfo.Debug(() => "WriteReturnIDsToSourceRecords: Zakończono");
        }

        private List<BulkCopy.BulkMethods.BulkWriteWithResult.ReturnIds<int>> MergeTempTableToDestination(SqlConnection sqlConnection, ColumnInformation.ColumnInfoCollection columnInfo, BulkResult result)
        {
            result.DebugInfo.Debug(() => "MergeTempTableToDestination: Generowanie SQL");
            var columnsNames = columnInfo.AllColumns.Select(x => x.Name);
            var columnsPrimaryKey = columnInfo.AllColumns.Where(x => x.IsPrimaryKey).Select(x => x.Name);
            string sql = Params.SqlGenerator.CreateMergeFromTempTableToDestinationTable(_tempTableName, Params.TableNameWithSchema, _timeStampColumnName, columnsNames, columnsPrimaryKey, Params.ReadIdsInsertedRecords);
            result.DebugInfo.Debug(() => "MergeTempTableToDestination: Wygenerowany SQL: (" + sql + ")");

            if (Params.ReadIdsInsertedRecords)
            {

                var updateIds = new List<BulkCopy.BulkMethods.BulkWriteWithResult.ReturnIds<int>>();
                result.DebugInfo.Debug(() => "MergeTempTableToDestination: Odczytywanie id-ków zwrotnych z MERGE...");

                Params.SqlQueryExecutor.ExexuteSQLWithReader(sql, sqlConnection, (reader) =>
                {
                    updateIds.Add(new BulkCopy.BulkMethods.BulkWriteWithResult.ReturnIds<int>((int)reader[0], (long)reader[1]));
                });

                result.DebugInfo.Debug(() => string.Format("MergeTempTableToDestination: Odczytywanie id-ków zwrotnych z MERGE...Zakończono. Odczytano {0} rekordów", updateIds.Count));
                return updateIds;
            }
            else
            {
                Params.SqlQueryExecutor.ExecuteSQL(sql, sqlConnection);
                return new List<BulkCopy.BulkMethods.BulkWriteWithResult.ReturnIds<int>>();
            }
        }

        private void ExecuteBulk(SqlConnection conn, DataTable dataTable, string tempTableName, SqlBulkCopyOptions options)
        {
            SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, options, null);
            bulkCopy.BatchSize = 30000;
            bulkCopy.BulkCopyTimeout = timeoutSeconds;
            bulkCopy.DestinationTableName = tempTableName;
            bulkCopy.WriteToServer(dataTable);
        }

        public int GetTimeout()
        {
            return timeoutSeconds;
        }

        public void SetTimeout(int numberOfSeconds)
        {
            timeoutSeconds = numberOfSeconds;
        }
    }
}
