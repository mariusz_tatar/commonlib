﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGVolt.Data.Bulk.BulkMethods
{
    public interface IBulkMethod<TRecord>
    {
        int GetTimeout();
        void SetTimeout(int numberOfSeconds);
        bool IsCanUse(RecordsCollection<TRecord> recordsCollection);
        void WriteToSerwer(RecordsCollection<TRecord> recordsCollection, BulkResult result, SqlConnection connection);
    }
}
